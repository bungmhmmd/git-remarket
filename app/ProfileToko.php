<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProfileToko extends Model
{
    use Notifiable;
    protected $guarded = [];
    protected $table = "profiletoko";

    public function district()
    {
        return $this->belongsTo(District::class);
    }
}
