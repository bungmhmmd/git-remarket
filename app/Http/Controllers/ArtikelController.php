<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use App\User;
use Validator,Redirect,Response,File;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ArtikelController extends Controller
{
    //admin
    public function index()
    {
        $artikel = Artikel::orderBy('created_at', 'DESC')->paginate(4);
        $user_id = auth()->user()->id;
        $artikel = Artikel::where('user_id', $user_id)->paginate(10);
    	return view('artikel.index')->with(compact('artikel'));
    }

    public function tambahartikel()
    {
    	$artikel = Artikel::get();
    	return view('artikel.post_textarea',[
    		'artikel' => $artikel
    	]);
    }

    public function store(Request $request)
    {
    	$artikel = new Artikel;
        $artikel->title = $request->title;
        $artikel->content = $request->content;
        $artikel->user_id = auth()->user()->id;
        $artikel->name = auth()->user()->name;
        if($request->hasFile('thumbnail')) {

            $file = $request->file('thumbnail');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
            $artikel->thumbnail = url('/images/' . $name);

        }
        $artikel->save();
    	return redirect(route('artikel.index'))->with(['success' => 'Artikel Ditambah']);
    }

    public function hapusartikel(Request $request)
    {
        $artikel = Artikel::find($request->get('id'));
        $thumbnail = \Illuminate\Support\Str::after($artikel->thumbnail, 'http://127.0.0.1:8000/images/');
        $image_path = public_path().'/images/' . $thumbnail;  // Value is not URL but directory file path
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        $artikel->delete();
        return redirect(route('artikel.index'))->with(['success' => 'artikel Sudah Dihapus']);
    }

    public function editartikel($id)
    {
        $artikel = Artikel::find($id);
        $thumbnail = \Illuminate\Support\Str::after($artikel->thumbnail, 'http://127.0.0.1:8000/images/');
        return view('artikel.edit')->with(compact('artikel','thumbnail'));
    }
    public function updateartikel(Request $request, $id)
    {
        
        $artikel = Artikel::find($id);
        
        
        if($request->hasFile('thumbnail')) {
            $thumbnail = \Illuminate\Support\Str::after($artikel->thumbnail, 'http://127.0.0.1:8000/images/');
            $image_path = public_path().'/images/' . $thumbnail;  // Value is not URL but directory file path
            if(File::exists($image_path)) {
            File::delete($image_path);
            }
            $file = $request->file('thumbnail');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();
            $file->move(public_path().'/images/', $name);
            $artikel->thumbnail = url('/images/' . $name);

        }
        $artikel->update([
            'title' => $request->title,
            'content' => $request->content,
        ]);
        return redirect(route('artikel.index'))->with(['success' => 'Artikel Diperbaharui']);
    }

    //ecommerce
    public function list_artikel()
    {
        $artikel = Artikel::orderBy('created_at', 'DESC')->paginate(4);
        //$artikels = $artikels->paginate(1);
    	return view('ecommerce.artikel')->with(compact('artikel'));
    }

    public function getFullPost($artikel_id) {
        $artikel = Artikel::find($artikel_id);
        return view('ecommerce.read')->with(compact('artikel'));
    }


    public function createPost(Request $request){
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(300,300)->save(public_path('uploads/image' . $filename));
            $post = Auth::user();
            $post->image = $filename;
            $post->save();
            $post = Post::create(array(
            'title' => Input::get('title'),
            'description' => Input::get('description'),
            'author' => Auth::user()->id
        
        ));

        }    
        return redirect()->route('home', array('post' =>  Auth::user()))->with('success', 'Post has been successfully added!');
    }

}
