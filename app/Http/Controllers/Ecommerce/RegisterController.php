<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Province;
use App\City;
use App\District;
use App\Customer;
use Illuminate\Support\Str;
use DB;
use App\Mail\CustomerRegisterMail;
use Mail;
use Cookie;

class RegisterController extends Controller
{
    public function registerForm()
    {
        if (auth()->guard('customer')->check()) return redirect(route('customer.dashboard'));
        $provinces = Province::orderBy('created_at', 'DESC')->get();
        return view('ecommerce.register', compact('provinces'));
    }

    public function checkout()
    {
        $provinces = Province::orderBy('created_at', 'DESC')->get();
        return view('ecommerce.checkout', compact('provinces'));
    }
    
    public function getCity()
    {
        $cities = City::where('province_id', request()->province_id)->get();
        return response()->json(['status' => 'success', 'data' => $cities]);
    }

    public function getDistrict()
    {
        $districts = District::where('city_id', request()->city_id)->get();
        return response()->json(['status' => 'success', 'data' => $districts]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'customer_name' => 'required|string|max:100',
            'customer_phone' => 'required',
            'email' => 'required|email',
            'customer_address' => 'required|string',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:cities,id',
            'district_id' => 'required|exists:districts,id',
            'password' => 'required'
        ]);
        
        try {
                if (!auth()->guard('customer')->check()) {
                    $customer = Customer::create([
                        'name' => $request->customer_name,
                        'email' => $request->email,
                        'phone_number' => $request->customer_phone,
                        'address' => $request->customer_address,
                        'district_id' => $request->district_id,
                        'password' => $request->password,
                        'activate_token' => Str::random(30),
                        'status' => true
                    ]);
                    session()->flash('regis_m', true);
                    return redirect()->intended(route('customer.login'));
                }

            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->with(['error' => $e->getMessage()]);
            }
    }

    
}
