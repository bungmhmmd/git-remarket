<?php

namespace App\Http\Controllers\Ecommerce;

use App\Http\Controllers\Controller;
use App\ProfileToko;
use App\RekBank;
use App\User;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Customer;
use App\Province;
use App\Order;
use App\Wishlist;
use App\Artikel;
use App\Pesan;
use App\Aktivasi_toko;
use Illuminate\Support\Str;
use File;
use Illuminate\Support\Facades\DB;
use App\Mail\OrderMail;
use Mail;
use Carbon\Carbon;
use PDF;

class FrontController extends Controller
{
    public function index()
    {

        $products = Product::orderBy('created_at', 'DESC')->paginate(10);
        $artikel = Artikel::orderBy('created_at', 'DESC')->paginate(10);
        return view('ecommerce.index', compact('products','artikel'));
    }

    public function product()
    {
        $products = Product::orderBy('created_at', 'DESC');
        if (request()->sorting == '1') {
            session(['sorting' => request()->sorting]);
        }
        if (request()->sorting == '2') {
            session(['sorting' => request()->sorting]);
            $products = Product::orderBy('created_at', 'ASC');
        }
        if (request()->q == '') {
            session()->forget(['q', 'sorting', 'show']);
        }
        if (request()->q != '') {
            session(['q' => request()->q]);
            $products = $products->where('name', 'LIKE', '%' . request()->q . '%')
                ->OrWhere('nama_toko', 'LIKE', '%' . request()->q . '%');
        }
        if (request()->show == '1') {
            session(['show' => request()->show]);
        }
        if (request()->show == '2') {
            session(['show' => request()->show]);
            $products = $products->paginate(14);
            $products->withPath('http://127.0.0.1:8000/product?sorting='. request()->sorting .'&show='. request()->show .'&submit=&');
            return view('ecommerce.product', compact('products'));
        }
        if (request()->show == '3') {
            session(['show' => request()->show]);
            $products = $products->paginate(16);
            $products->withPath('http://127.0.0.1:8000/product?sorting='. request()->sorting .'&show='. request()->show .'&submit=&');
            return view('ecommerce.product', compact('products'));
        }
        $products = $products->paginate(12);
        return view('ecommerce.product', compact('products'));
    }

    /*public function kategori_pilihan($category)
    {
        $products = Category::where('slug', $slug)->first()->product()->orderBy('created_at', 'DESC')->paginate(12);
        return view('ecommerce.product', compact('products'));
    }*/



    public function categoryProduct($slug)
    {
        $products = Category::where('slug', $slug)->first()->product()->orderBy('created_at', 'DESC')->paginate(12);
        return view('ecommerce.product', compact('products'));
    }

    public function show($slug)
    {
        $product = Product::with(['category'])->where('slug', $slug)->first();
        $statustoko = Customer::where('username_toko','=',$product->username_toko)->first();
        return view('ecommerce.show', compact('product','statustoko'));
    }
    //profile toko
    public function showtoko($username_toko,$slug)
    {
        $profile_toko = ProfileToko::where('username_toko',$username_toko)->first()->load('district');
        $statustoko = Customer::where('username_toko','=',$username_toko)->first();
        if ($slug == "all"){
            $products = Product::where('username_toko',$username_toko)->orderBy('created_at', 'DESC')->paginate(12);
        }elseif ($slug == "terlama"){
            $products = Product::where('username_toko',$username_toko)->orderBy('created_at', 'ASC')->paginate(12);
        }elseif ($slug == "14"){
            $products = Product::where('username_toko',$username_toko)->orderBy('created_at', 'DESC')->paginate(14);
        }elseif ($slug == "16"){
            $products = Product::where('username_toko',$username_toko)->orderBy('created_at', 'DESC')->paginate(16);
        }else{
            $products = Category::where('slug', $slug)->first()->product()
                ->where('username_toko',$username_toko)
                ->orderBy('created_at', 'DESC')->paginate(12);
        }
        $provinces = Province::orderBy('name', 'ASC')->get();
        $customer = ProfileToko::where('username_toko','=',$username_toko)->first()->load('district');

        return view('ecommerce.toko', compact('products','profile_toko','provinces','customer','statustoko'));
    }


    public function verifyCustomerRegistration($token)
    {
        $customer = Customer::where('activate_token', $token)->first();
        if ($customer) {
            $customer->update([
                'activate_token' => null,
                'status' => 1
            ]);
            return redirect(route('customer.login'))->with(['success' => 'Verifikasi Berhasil, Silahkan Login']);
        }
        return redirect(route('customer.login'))->with(['error' => 'Invalid Verifikasi Token']);
    }

    public function customerSettingForm()
    {
        $customer = auth()->guard('customer')->user()->load('district');
        $provinces = Province::orderBy('name', 'ASC')->get();
        return view('ecommerce.setting', compact('customer', 'provinces'));
    }

    public function customerUpdateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'phone_number' => 'required|max:15',
            'email' => 'required|string',
            'address' => 'required|string',
            'district_id' => 'required|exists:districts,id',
            'password' => 'nullable|string|min:6'
        ]);

        $user = auth()->guard('customer')->user();
        $data = $request->only('name','email', 'phone_number', 'address', 'district_id');
        if ($request->password != '') {
            $data['password'] = $request->password;
        }
        $user->update($data);
        return redirect()->back()->with(['success' => 'Profil berhasil diperbaharui']);
    }

    public function referalProduct($user, $product)
    {
        $code = $user . '-' . $product;
        $product = Product::find($product);
        $cookie = cookie('dw-afiliasi', json_encode($code), 2880);
        return redirect(route('front.show_product', $product->slug))->cookie($cookie);
    }

    public function listCommission()
    {
        $user = auth()->guard('customer')->user();
        $orders = Order::where('ref', $user->id)->where('status', 4)->paginate(10);
        return view('ecommerce.affiliate', compact('orders'));
    }

    //toko
    public function tokosaya()
    {
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $product = Product::where('username_toko','=',$user->username_toko)
        ->with(['category'])->orderBy('created_at', 'DESC');
        if (request()->q != '') {
            $product = $product->where('name', 'LIKE', '%' . request()->q . '%');
        }
        $product = $product->paginate(10);
        $orders = Order::
            where('username_toko','=',auth()->guard('customer')->user()->username_toko)
            ->with(['customer.district.city.province'])
            ->withCount('return')
            ->orderBy('created_at', 'DESC');

        if (request()->q != '') {
            $orders = $orders->where(function($q) {
                $q->where('username_toko','=',auth()->guard('customer')->user()->username_toko)
                    ->where('customer_name', 'LIKE', '%' . request()->q . '%')
                    ->orWhere('invoice', 'LIKE', '%' . request()->q . '%')
                    ->orWhere('customer_address', 'LIKE', '%' . request()->q . '%');
            });
        }

        if (request()->status != '') {
            $orders = $orders->where('status', request()->status);
        }
        $orders = $orders->paginate(10);

        $start = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
        $end = Carbon::now()->endOfMonth()->format('Y-m-d H:i:s');

        if (request()->date != '') {
            $date = explode(' - ' ,request()->date);
            $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
            $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';
        }

        $laporanorders = Order::with(['customer.district'])
            ->where('username_toko','=',auth()->guard('customer')->user()->username_toko)
            ->whereBetween('created_at', [$start, $end])
            ->get();
        $laporanreturn = Order::with(['customer.district'])
            ->where('username_toko','=',auth()->guard('customer')->user()->username_toko)
            ->has('return')->whereBetween('created_at', [$start, $end])
            ->get();

        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first();
        return view('ecommerce.tokosaya.tokosaya', compact('product','user','orders','laporanorders','laporanreturn','profiletoko'));
    }
    //aktivasi toko
    public function aktivasitoko()
    {
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $aktivasitoko = Aktivasi_toko::where('customer_id','=',$user->id)->first();
        return view('ecommerce.tokosaya.aktivasitoko', compact('user','aktivasitoko'));
    }

    public function kirimaktivasi(Request $request)
    {
        $rules = [
            'toko_username' => ['required', 'max:15','regex:/^\S*$/u'],
            'toko_name' => 'required|max:55',
        ];
        $messages = [
            'toko_username.regex'   => 'username tanpa spasi',
        ];
        $this->validate($request, [
            'toko_name' => 'required|max:55',
            'description' => 'required',
            'toko_username' => ['required', 'max:15','regex:/^\S*$/u'],
            'image' => 'required|image|mimes:png,jpeg,jpg'
        ]);
        
        $check = Aktivasi_toko::where('toko_username','=',$request->get('toko_username'))->first();
        if (!empty($check)){
            return redirect()->back()->with(['error' => 'username toko sudah ada!']);
        }

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . Str::slug($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/aktivasi_toko', $filename);
            $status_toko = '0';
            $aktivasi_toko = Aktivasi_toko::create([
                'toko_name' => $request->toko_name,
                'toko_username' => $request->toko_username,
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name,
                'description' => $request->description,
                'image' => $filename,
                'status_toko' => $status_toko
            ]);
        $user = Customer::where('id','=',auth()->guard('customer')->user()->id)->first();
        $user->status_toko = 2;
        $user->save();
        return redirect()->back()->with(['success' => 'Permintaan Terkirim']);
        }
    }
    //aktifkan toko
    public function aktifkantoko(Request $request)
    {
        $rules = [
            'username_toko' => ['required', 'max:15','regex:/^\S*$/u'],
            'nama_toko' => 'required|max:55',
        ];
        $messages = [
            'username_toko.regex'   => 'username tanpa spasi',
        ];
        $this->validate($request, [
            'username_toko' => ['required', 'max:15','regex:/^\S*$/u'],
            'nama_toko' => 'required|max:55',
        ]);

        $check = Customer::where('username_toko','=',$request->get('username_toko'))->first();
        if (!empty($check)){
            return redirect()->back()->with(['error' => 'username toko sudah ada!']);
        }
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();

        $user->status_toko = 2;
        $user->nama_toko = $request->get('nama_toko');
        $user->username_toko = $request->get('username_toko');
        $user->save();

        $profiletoko = New ProfileToko;
        $profiletoko->nama_toko = $request->get('nama_toko');
        $profiletoko->deskripsi_toko = $request->get('deskripsi_toko');
        $profiletoko->username_toko = $request->get('username_toko');
        $profiletoko->district_id = 0;
        $profiletoko->save();
        return redirect()->back()->with(['success' => 'Toko berhasil diaktifkan']);
    }

    //profil toko
    public function profiletoko()
    {
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        if ($user->status_toko == 0){
            return redirect(route('customer.tokosaya'))->with(['error' => 'toko belum di aktifkan!']);
        }elseif ($user->status_toko == 2) {
            return redirect(route('customer.tokosaya'));
        }
        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first()->load('district');
        $customer = ProfileToko::where('username_toko','=',$user->username_toko)->first()->load('district');
        $provinces = Province::orderBy('name', 'ASC')->get();

        $bank = RekBank::where('username_toko','=',$user->username_toko)->get();

        return view('ecommerce.tokosaya.profiletoko', compact('user','profiletoko','provinces','customer','bank'));
    }

    public function profiletokoUpdateProfile(Request $request)
    {
        $this->validate($request, [
            'nama_toko' => 'required|string|max:100',
            'phone_number' => 'required|max:15',
            'address' => 'required|string',
            'district_id' => 'required|exists:districts,id',
        ]);

        $profile_toko = ProfileToko::where('username_toko','=',$request->get('username_toko'))->first();

        if ($request->get('nama_toko') !== null){
            $profile_toko->nama_toko = $request->get('nama_toko');
        }
        if ($request->get('phone_number') !== null){
            $profile_toko->phone_number = $request->get('phone_number');
        }
        if ($request->get('address') !== null){
            $profile_toko->address = $request->get('address');
        }
        if ($request->get('deskripsi_toko') !== null){
            $profile_toko->deskripsi_toko = $request->get('deskripsi_toko');
        }
        if ($request->get('district_id') !== null){
            $profile_toko->district_id = $request->get('district_id');
        }
        $profile_toko->save();
        return redirect()->back()->with(['success' => 'Profil berhasil diperbaharui']);
    }

    public function createproduk()
    {
        $category = Category::orderBy('name', 'DESC')->get();
        return view('ecommerce.tokosaya.products.create', compact('category'));
    }

    public function storeproduk(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'required|image|mimes:png,jpeg,jpg'
        ]);
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . Str::slug($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/products', $filename);

            $product = Product::create([
                'name' => $request->name,
                'slug' => $request->name,
                'category_id' => $request->category_id,
                'description' => $request->description,
                'image' => $filename,
                'price' => $request->price,
                'weight' => $request->weight,
                'status' => $request->status,
                'nama_toko' =>$user->nama_toko,
                'username_toko' =>$user->username_toko,
            ]);
            return redirect(route('customer.tokosaya'))->with(['success' => 'Produk Baru Ditambahkan']);
        }
    }

    public function hapusproduk(Request $request)
    {
        $product = Product::find($request->get('id'));
        File::delete(storage_path('app/public/products/' . $product->image));
        $product->delete();
        return redirect(route('customer.tokosaya'))->with(['success' => 'Produk Sudah Dihapus']);
    }

    public function editproduk($id)
    {
        $product = Product::find($id);
        $category = Category::orderBy('name', 'DESC')->get();
        return view('ecommerce.tokosaya.products.edit', compact('product', 'category'));
    }
    public function updateproduk(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'nullable|image|mimes:png,jpeg,jpg'
        ]);

        $product = Product::find($id);
        $filename = $product->image;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . Str::slug($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/products', $filename);
            File::delete(storage_path('app/public/products/' . $product->image));
        }

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'weight' => $request->weight,
            'image' => $filename
        ]);
        return redirect(route('customer.tokosaya'))->with(['success' => 'Data Produk Diperbaharui']);
    }

    //pesanan

    public function view($invoice)
    {
        $order = Order::with(['customer.district.city.province', 'payment', 'details.product'])->where('invoice', $invoice)->first();
        return view('ecommerce.tokosaya.orderstoko.view', compact('order'));
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        $order->details()->delete();
        $order->payment()->delete();
        $order->delete();
        return redirect(route('customer.tokosaya'));
    }

    public function acceptPayment($invoice)
    {
        $order = Order::with(['payment'])->where('invoice', $invoice)->first();
        $order->payment()->update(['status' => 1]);
        $order->update(['status' => 2]);
        return redirect(route('customer.tokosaya'));
    }

    public function shippingOrder(Request $request)
    {
        $order = Order::with(['customer'])->find($request->order_id);
        $order->update(['tracking_number' => $request->tracking_number, 'status' => 3]);
        Mail::to($order->customer->email)->send(new OrderMail($order));
        return redirect()->back();
    }

    public function return($invoice)
    {
        $order = Order::with(['return', 'customer'])->where('invoice', $invoice)->first();
        return view('ecommerce.tokosaya.orderstoko.return', compact('order'));
    }

    public function approveReturn(Request $request)
    {
        $this->validate($request, ['status' => 'required']);
        $order = Order::find($request->order_id);
        $order->return()->update(['status' => $request->status]);
        $order->update(['status' => 4]);
        return redirect()->back();
    }

    //wishlist
    public function addTowishlist(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'customer_id' => 'required|exists:customers,id',
        ]);

        $product = Product::find($request->product_id);
        $customer = Customer::find($request->customer_id);
        $wishlist = Wishlist::create([
            'product_id' => $product->id,
            'customer_id' => $customer->id,
            'product_name' => $product->name,
            'product_toko' => $product->nama_toko,
            'product_img' => $product->image,
            'product_id' => $product->id,
            'product_price' => $product->price,

        ]);
        return redirect()->back()->with(['success' => 'Produk Ditambahkan ke Wishlist']);
    }

    public function wishlist()
    {
        $customer_id = auth()->guard('customer')->user()->id;
        $wishlist = DB::table('wishlists')
            ->where('customer_id', $customer_id)
            ->orderBy('created_at', 'DESC')
            ->paginate();
        
        return view('ecommerce.wishlist', ['wishlist' => $wishlist]);
    }

    /*public function deletewishlist(Request $request)
    {
        $wishlist = Wishlist::find($request->get('id'));
        $wishlist->delete();
        return redirect(route('ecommerce.wishlist'));
    }*/


    public function deletewishlist(Request $request)
	{
        $this->validate($request, [
            'wishlist_id' => 'required|exists:wishlists,id',
        ]);
        $wishlist = Wishlist::find($request->wishlist_id);
        $wishlist->delete();

	return redirect()->back();
	}

    public function orderReportPdf($daterange)
    {
        $date = explode('+', $daterange);
        $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
        $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';

        $orders = Order::with(['customer.district'])
            ->where('username_toko','=',auth()->guard('customer')->user()->username_toko)
            ->whereBetween('created_at', [$start, $end])
            ->get();
        $pdf = PDF::loadView('ecommerce.tokosaya.laporan.order_pdf', compact('orders', 'date'));
        return $pdf->stream();
    }

    public function returnReportPdf($daterange)
    {
        $date = explode('+', $daterange);
        $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
        $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';

        $orders = Order::with(['customer.district'])->has('return')
            ->where('username_toko','=',auth()->guard('customer')->user()->username_toko)
            ->whereBetween('created_at', [$start, $end])
            ->get();
        $pdf = PDF::loadView('report.return_pdf', compact('orders', 'date'));
        return $pdf->stream();
    }


    public function hapusava(){

        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first();

        $path = public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->avatar;
        if(file_exists($path)) {
            unlink($path);
        }
        $profiletoko->avatar = NULL;
        $profiletoko->save();


        return response()->json(['responseText' => 'Success!']);
    }

    public function uploadava(Request $request){

        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first();

        if ($profiletoko->avatar !== null){
            $path = public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->avatar;
            if(file_exists($path)) {
                unlink($path);
            }
        }

        //uploudfile client
        if ($request->hasFile('uploadava')){
            $file = $request->file('uploadava');
            $filekecilin = strtolower($file->getClientOriginalExtension());
            if($filekecilin == "jpg"|| $filekecilin == "jpeg" || $filekecilin == "png"){

                if(!File::isDirectory(public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->avatar)) {
                    File::makeDirectory(public_path().'/tokosaya/'.$profiletoko->username_toko, 0777, true, true);
                }

                $name= $file->getClientOriginalName();
                $file->move(public_path().'/tokosaya/'.$profiletoko->username_toko, $name);

                $profiletoko->avatar = $name;
                $profiletoko->save();

            }else{
                $data= "nofile";
                return $data;
            }
        }else{
            $data= "nofile";
            return $data;
        }
        $data= $name;
        return $data;
    }

    public function hapusbanner(){

        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first();

        $path = public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->banner;
        if(file_exists($path)) {
            unlink($path);
        }
        $profiletoko->banner = NULL;
        $profiletoko->save();


        return response()->json(['responseText' => 'Success!']);
    }

    public function uploadbanner(Request $request){

        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();
        $profiletoko = ProfileToko::where('username_toko','=',$user->username_toko)->first();

        if ($profiletoko->banner !== null){
            $path = public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->banner;
            if(file_exists($path)) {
                unlink($path);
            }
        }

        //uploudfile client
        if ($request->hasFile('uploadbanner')){
            $file = $request->file('uploadbanner');
            $filekecilin = strtolower($file->getClientOriginalExtension());
            if($filekecilin == "jpg"|| $filekecilin == "jpeg" || $filekecilin == "png"){

                if(!File::isDirectory(public_path().'/tokosaya/'.$profiletoko->username_toko.'/'.$profiletoko->banner)) {
                    File::makeDirectory(public_path().'/tokosaya/'.$profiletoko->username_toko, 0777, true, true);
                }

                $name= $file->getClientOriginalName();
                $file->move(public_path().'/tokosaya/'.$profiletoko->username_toko, $name);

                $profiletoko->banner = $name;
                $profiletoko->save();

            }else{
                $data= "nofile";
                return $data;
            }
        }else{
            $data= "nofile";
            return $data;
        }
        $data= $name;
        return $data;
    }

    //bank
    public function createbank()
    {
        return view('ecommerce.tokosaya.bank.create');
    }

    public function storebank(Request $request)
    {
        $this->validate($request, [
            'pemilik_rek' => 'required|string|max:100',
            'no_rek' => 'required',
            'bank' => 'required',
        ]);
        $user = Customer::where('email','=',auth()->guard('customer')->user()->email)->first();

            $bank = RekBank::create([
                'atasnama' => $request->pemilik_rek,
                'no_rek' => $request->no_rek,
                'bank' => $request->bank,
                'username_toko' => $user->username_toko,
            ]);
            return redirect(route('customer.profiletoko'))->with(['success' => 'Nomor Rekening Bank Baru Ditambahkan']);

    }

    public function hapusbank(Request $request)
    {
        $bank = RekBank::find($request->get('id'));
        $bank->delete();
        return redirect(route('customer.profiletoko'))->with(['success' => 'Nomor Rekening Bank Sudah Dihapus']);
    }

    public function editbank($id)
    {
        $bank = RekBank::find($id);
        return view('ecommerce.tokosaya.bank.edit', compact('bank'));
    }
    public function updatebank(Request $request, $id)
    {
        $this->validate($request, [
            'pemilik_rek' => 'required|string|max:100',
            'no_rek' => 'required',
            'bank' => 'required',
        ]);

        $bank = RekBank::find($id);
        $bank->update([
            'atasnama' => $request->pemilik_rek,
            'no_rek' => $request->no_rek,
            'bank' => $request->bank,
        ]);
        return redirect(route('customer.profiletoko'))->with(['success' => 'Nomor Rekening Bank Produk Diperbaharui']);
    }

    //Kontak
    public function kontak()
    {
        return view('ecommerce.kontak');
    }

    public function addpesan(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|string|max:100',
            'email' => 'required|email',
            'subjek' => 'required',
            'pesan' => 'required',
        ]);
        
                    $pesan = Pesan::create([
                        'username' => $request->nama,
                        'email' => $request->email,
                        'subject' => $request->subjek,
                        'message' => $request->pesan,
                    ]);
                    
                    return redirect()->back()->with(['success' => 'Pesan telah Ditambahkan']);

               
    }

}
