<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Aktivasi_toko;
use App\Customer;
use App\ProfileToko;
use App\Mail\OrderMail;
use Mail;
use File;
use App\Mail\AktivasiTerimaMail;
use App\Mail\AktivasiTolakMail;


class AktivasiController extends Controller
{
    public function index()
    {
        $aktivasitoko = Aktivasi_toko::orderBy('created_at', 'DESC');
        
        if (request()->q != '') {
            $aktivasitoko = $aktivasitoko->where(function($q) {
                $q->where('customer_name', 'LIKE', '%' . request()->q . '%')
                ->orWhere('toko_name', 'LIKE', '%' . request()->q . '%')
                ->orWhere('toko_username', 'LIKE', '%' . request()->q . '%');
            });
        }

        if (request()->status != '') {
            $aktivasitoko = $aktivasitoko->where('status_toko', request()->status);
        }
        $aktivasitoko = $aktivasitoko->paginate(10);
        return view('aktivasi_toko.index', compact('aktivasitoko'));
    }

    public function detail($id)
    {
        $aktivasitoko = Aktivasi_toko::where('id', $id)->first();
        $customer = Customer::with(['district.city.province'])->where('id', $aktivasitoko->customer_id)->first();
        return view('aktivasi_toko.detail', compact('aktivasitoko','customer'));
    }

    public function hapus(Request $request)
    {
        $user = Customer::where('id','=',$request->get('customer_id'))->first();
        if ($user->status_toko != 1) {
            $user->status_toko = 0;
            $user->save();
        }
        $aktivasitoko = Aktivasi_toko::find($request->get('id'))->first();
        File::delete(storage_path('app/public/aktivasi_toko/' . $aktivasitoko->image));
        $aktivasitoko->delete();
        return redirect(route('aktivasitoko.index'))->with(['error' => 'Permintaan Aktivasi Sudah Dihapus']);;
    }

    public function terima(Request $request)
    {
        $aktivasitoko = Aktivasi_toko::where('id', $request->get('id'))->first();
        $aktivasitoko->status_toko = 1;
        $aktivasitoko->save();
        $user = Customer::where('id','=',$aktivasitoko->customer_id)->first();
        $user->status_toko = 1;
        $user->username_toko = $aktivasitoko->toko_username;
        $user->nama_toko = $aktivasitoko->toko_name;
        $user->save();
        Mail::to($user->email)->send(new AktivasiTerimaMail());
        //Mail::to("bungmohammad@gmail.com")->send(new AktivasiTerimaMail());
        
        $profiletoko = New ProfileToko;
        $profiletoko->nama_toko = $aktivasitoko->toko_name;
        $profiletoko->deskripsi_toko = $aktivasitoko->description;
        $profiletoko->username_toko = $aktivasitoko->toko_username;
        $profiletoko->district_id = $user->district_id;
        $profiletoko->phone_number = $user->phone_number;
        $profiletoko->address = $user->address;
        $profiletoko->save();
        return redirect(route('aktivasitoko.index'))->with(['success' => 'Permintaan Aktivasi diterima']);;
    }

    public function tolak(Request $request)
    {
        $aktivasitoko = Aktivasi_toko::where('id', $request->get('id'))->first();
        $aktivasitoko->status_toko = 2;
        $aktivasitoko->toko_username = ' ';
        $aktivasitoko->save();
        $user = Customer::where('id','=',$aktivasitoko->customer_id)->first();
        $user->status_toko = 3;
        $user->save();
        Mail::to($user->email)->send(new AktivasiTolakMail());
        //Mail::to("bungmohammad@gmail.com")->send(new AktivasiTolakMail());
        
        /*$profiletoko = New ProfileToko;
        $profiletoko->nama_toko = $aktivasitoko->toko_name;
        $profiletoko->deskripsi_toko = $aktivasitoko->description;
        $profiletoko->username_toko = $aktivasitoko->toko_username;
        $profiletoko->district_id = 0;
        $profiletoko->save();*/
        return redirect(route('aktivasitoko.index'))->with(['error' => 'Permintaan Aktivasi ditolak']);;
    }

    public function acceptPayment($invoice)
    {
        $aktivasitoko = Order::with(['payment'])->where('invoice', $invoice)->first();
        $aktivasitoko->payment()->update(['status' => 1]);
        $aktivasitoko->update(['status' => 2]);
        return redirect(route('orders.view', $aktivasitoko->invoice));
    }

    public function shippingOrder(Request $request)
    {
        $aktivasitoko = Order::with(['customer'])->find($request->order_id);
        $aktivasitoko->update(['tracking_number' => $request->tracking_number, 'status' => 3]);
        Mail::to($aktivasitoko->customer->email)->send(new OrderMail($aktivasitoko));
        return redirect()->back();
    }

    public function return($invoice)
    {
        $aktivasitoko = Order::with(['return', 'customer'])->where('invoice', $invoice)->first();
        return view('orders.return', compact('order'));
    }

    public function approveReturn(Request $request)
    {
        $this->validate($request, ['status' => 'required']);
        $aktivasitoko = Order::find($request->order_id);
        $aktivasitoko->return()->update(['status' => $request->status]);
        $aktivasitoko->update(['status' => 4]);
        return redirect()->back();
    }
}
