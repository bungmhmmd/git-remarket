@extends('layouts.admin')

@section('title')
    <title>Artikel</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">Artikel</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Artikel {{ $thumbnail }}</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('administrator/artikel/updateartikel/' . $artikel->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <h4 style="color: black;" >Judul</h4>
                                    <br>
                                    <input type="text" class="form-control" name="title" value="{{ $artikel->title }}">
                                </div>
                                <div class="form-group">
                                    <h4 style="color: black;" >Thumbnail artikel</h4>
                                    <br>
                                    <img src="{{  $artikel->thumbnail }}" width="100px" height="100px" alt="{{ $artikel->title }}">
                                    <hr>
                                    <input type="file" name="thumbnail" class="form-control">
                                    <p><strong>Biarkan kosong jika tidak ingin mengganti thumbnail</strong></p>
                                </div>
                                <div class="form-group">
                                    <h4 style="color: black;" >Isi</h4>
                                    <br>
                                        <textarea class="form-control" name="content" id="" rows="10">{{ $artikel->content }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-md" type="submit" >Update</button>
                                </div>                     
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>        
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
