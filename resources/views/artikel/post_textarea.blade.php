@extends('layouts.admin')

@section('title')
    <title>Artikel</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">Artikel</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('artikel.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                
                                <div class="form-group">
                                    <h4 style="color: black;" >Judul</h4>
                                    <br>
                                    <input type="text" class="form-control" name="title">
                                </div> 
                                <div class="form-group">
                                    <h4 style="color: black;" >Thumbnail</h4>
                                    <br>
                                    <input type="file" name="thumbnail">
                                </div>                     
                                <div class="form-group">
                                    <h4 style="color: black;" >Isi</h4>
                                    <br>
                                        <textarea class="form-control" name="content" id="" rows="10"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-md" type="submit" >Upload</button>
                                </div>                     
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>        
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
