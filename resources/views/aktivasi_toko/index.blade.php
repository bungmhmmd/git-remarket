@extends('layouts.admin')

@section('title')
    <title>Daftar Permintaan Aktivasi Toko</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">Aktivasi Toko</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Daftar Permintaan Aktivasi Toko
                            </h4>
                        </div>
                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success">{{ session('success') }}</div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif

                            <form action="{{ route('aktivasitoko.index') }}" method="get">
                                <div class="input-group mb-3 col-md-6 float-right">
                                    <select name="status" class="form-control mr-3">
                                        <option value="">Pilih Status</option>
                                        <option value="0">Baru</option>
                                        <option value="1">Diterima</option>
                                        <option value="2">Ditolak</option>
                                    </select>
                                    <input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">Cari</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Nama Customer</th>
                                            <th>Nama Toko</th>
                                            <th>Deskripsi toko</th>
                                            <th>Foto Produk</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($aktivasitoko as $row)
                                        <tr>
                                            <td><strong>{{ $row->customer_name }}</strong></td>
                                            <td>
                                                <strong>{{ $row->toko_name }}</strong><br>
                                                <label><strong>Username:</strong> {{ $row->toko_username }}</label><br>                                                
                                            </td>
                                            <td>{!! \Illuminate\Support\Str::limit($row->description, $limit = 200, $end = '...</p>') !!}</td>
                                            <td>
                                                <img src="{{ asset('storage/aktivasi_toko/' . $row->image) }}" width="100px" height="100px">
                                            </td>
                                            <td>
                                                @if($row->status_toko == 0)
                                                    Baru<br>
                                                @elseif($row->status_toko == 1)
                                                    Diterima<br>
                                                @else
                                                    Ditolak<br>
                                                @endif
                                            </td>
                                            <td>
                                                    <a href="{{ route('aktivasitoko.detail', $row->id) }}" class="btn btn-warning btn-sm">Lihat</a>
                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#konfirmasi">Hapus</button>
                                                    <!-- Modal -->
                                                        <div id="konfirmasi" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                                <!-- konten modal-->
                                                                <div class="modal-content">
                                                                    <!-- heading modal -->
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Hapus Permintaan Aktivasi</h4>
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>                                                                        
                                                                    </div>
                                                                    <!-- body modal -->
                                                                    <div class="modal-body">
                                                                        <p>Apakah anda yakin untuk meghapus permintaan aktivasi ini?</p>
                                                                    </div>
                                                                    <!-- footer modal -->
                                                                    <div class="modal-footer">
                                                                    <form action="{{ route('aktivasitoko.hapus') }}" method="post">
                                                                    @csrf
                                                                        <input name="id" type="hidden" value="{{ $row->id }}">
                                                                        <input name="customer_id" type="hidden" value="{{ $row->customer_id }}">
                                                                        <button class="btn btn-danger btn-md">Iya</button>
                                                                        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Tidak</button>
                                                                    </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="6" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {!! $aktivasitoko->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
