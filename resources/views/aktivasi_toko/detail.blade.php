@extends('layouts.admin')

@section('title')
    <title>Detail Aktivasi Toko</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Dashboard</li>
        <li class="breadcrumb-item active">Detail Aktivasi Toko</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                Detail Aktivasi Toko

                                <div class="float-right">
                                    @if ($aktivasitoko->status == 1 && $aktivasitoko->payment->status == 0)
                                    <a href="{{ route('orders.approve_payment', $aktivasitoko->invoice) }}" class="btn btn-primary btn-sm">Terima Pembayaran</a>
                                    @endif
                                </div>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Detail Customer</h4>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%">Nama Customer</th>
                                            <td>{{ $customer->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Telp</th>
                                            <td>{{ $customer->phone_number }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $customer->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>{{ $customer->address }} {{ $customer->district->name }} - {{  $customer->district->city->name}}, {{ $customer->district->city->province->name }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h4>Detail Toko</h4>
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="30%">Nama Toko</th>
                                            <td>{{ $aktivasitoko->toko_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Username</th>
                                            <td>{{ $aktivasitoko->toko_username }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal dan Jam Mengirim Permintaan Aktivasi</th>
                                            <td>{{ $aktivasitoko->created_at }}</td>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                                @if($aktivasitoko->status_toko == 0)
                                                    <td> Baru </td>
                                                @elseif($aktivasitoko->status_toko == 1)
                                                    <td> Diterima</td>
                                                @else
                                                    <td> Ditolak</td>
                                                @endif                
                                        </tr>
                                        @if($aktivasitoko->status_toko != 2)
                                        <tr>
                                            <th>Aksi</th>
                                            <td>
                                                <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#konfirmasi_terima">Terima</button>
                                                        <!-- Modal -->
                                                            <div id="konfirmasi_terima" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <!-- konten modal-->
                                                                    <div class="modal-content">
                                                                        <!-- heading modal -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Terima Permintaan Aktivasi</h4>
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>                                                                        
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body">
                                                                            <p>Apakah anda yakin untuk menerima permintaan aktivasi ini?</p>
                                                                        </div>
                                                                        <!-- footer modal -->
                                                                        <div class="modal-footer">
                                                                            <form action="{{ route('aktivasitoko.terima') }}" method="post">
                                                                            @csrf
                                                                                <input name="id" type="hidden" value="{{ $aktivasitoko->id }}">
                                                                                <button class="btn btn-success btn-md">Iya</button>
                                                                                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Tidak</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                <br>
                                                <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#konfirmasi_tolak">Tolak</button>
                                                        <!-- Modal -->
                                                            <div id="konfirmasi_tolak" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <!-- konten modal-->
                                                                    <div class="modal-content">
                                                                        <!-- heading modal -->
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Tolak Permintaan Aktivasi</h4>
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>                                                                        
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body">
                                                                            <p>Apakah anda yakin untuk menolak permintaan aktivasi ini?</p>
                                                                        </div>
                                                                        <!-- footer modal -->
                                                                        <div class="modal-footer">
                                                                            <form action="{{ route('aktivasitoko.tolak') }}" method="post">
                                                                            @csrf
                                                                                <input name="id" type="hidden" value="{{ $aktivasitoko->id }}">   
                                                                                <button class="btn btn-danger btn-md">Iya</button>
                                                                                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">Tidak</button>
                                                                            </form>
                                                                         </div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                            </td>
                                        </tr>
                                        @endif
                                    </table>                                    
                                </div>
                                <div class="col-md-12">
                                    <h4>Detail Toko</h4>
                                    <table class="table table-borderd table-hover">
                                        <tr>
                                            <th>Foto Produk</th>
                                            <th>Deskripsi Toko</th>
                                        </tr>
                                        <tr>
                                            <td><img src="{{ asset('storage/aktivasi_toko/' . $aktivasitoko->image) }}" width="300px" height="300px"></td>
                                            <td>{!! $aktivasitoko->description !!}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
