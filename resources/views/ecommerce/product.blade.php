@extends('layouts.ecommerce')

@section('title')
    <title>Produk - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
            <div class="overlay"></div>
                <div class="banner_content text-center">
                    <h2>Produk</h2>
                    <div class="page_link">
                        <a href="{{ route('front.index') }}">Beranda</a>
                        <a href="{{ route('front.product') }}">Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area section_gap">
        <div class="container-fluid">
            <div class="row flex-row-reverse">
                <div class="col-lg-9">
                    <div class="product_top_bar">
                        <div class="left_page">
                        <form action="{{ route('front.product') }}" method="get">
                            <div class="input-group">
                                <input type="hidden" name="q" value="{{ session('q') }}" class="form-control">
                                <select class="sorting" name="sorting" >
                                @if( request()->sorting == '2')
                                    <option value="2">Diurut dari produk terlama</option>
                                    <option value="1">Diurut dari produk terbaru</option>
                                @else
                                    <option value="1">Diurut dari produk terbaru</option>
                                    <option value="2">Diurut dari produk terlama</option>
                                @endif
                                </select>

                                <select class="show" name="show" value="{{ request()->show }}">
                                @if( request()->show == '2')
                                    <option value="2">Tampilkan 14</option>
                                    <option value="1">Tampilkan 12</option>
                                    <option value="3">Tampilkan 16</option>
                                @elseif( request()->show == '3')
                                    <option value="3">Tampilkan 16</option>
                                    <option value="2">Tampilkan 14</option>
                                    <option value="1">Tampilkan 12</option>
                                @else
                                    <option value="1">Tampilkan 12</option>
                                    <option value="2">Tampilkan 14</option>
                                    <option value="3">Tampilkan 16</option>
                                @endif
                                </select>
                                <button class="btn" type="submit" name="submit">Atur</button>
                            </div>
                        </form>
                        </div>
                        <div class="right_page ml-auto">
                            {{ $products->links() }}
                        </div>
                    </div>
                    <div class="latest_product_inner row">
                        @forelse ($products as $row)
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="f_p_item">
                                <div class="f_p_img">
                                    <img class="img-fluid" src="{{ asset('storage/products/' . $row->image) }}" alt="{{ $row->name }}">
                                    <div class="p_icon">
                                        <a href="{{ url('/product/' . $row->slug) }}">
                                            <i class="lnr lnr-cart"></i>
                                        </a>
                                    </div>
                                </div>
                                <a href="{{ url('/product/' . $row->slug) }}">
                                    <h4>{{ $row->name }}</h4>                                    
                                </a>
                                <a class="active" href="{{ url('/toko/' . $row->username_toko.'/all') }}">{{ $row->nama_toko }}</a>
                                <h5>Rp {{ number_format($row->price) }}</h5>
                            </div>
                        </div>
                        @empty
                        <div class="col-md-12">
                            <h3 class="text-center">Tidak ada produk</h3>
                        </div>
                        @endforelse
                    </div>
                    <br><br>
                    <div class="product_top_bar">                       
                        <div class="right_page ml-auto">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets cat_widgets">
                            <div class="l_w_title">
                                <h3>Kategori Produk</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list" >
                                    @foreach ($categories as $category)
                                    <li>
                                        <strong><a href="{{ url('/category/' . $category->slug) }}">{{ $category->name }}</a></strong>
                                        
                                        @foreach ($category->child as $child)
                                        <ul class="list" style="display: block">
                                            <li>
                                                <a href="{{ url('/category/' . $child->slug) }}">{{ $child->name }}</a>
                                            </li>
                                        </ul>
                                        @endforeach
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

            
        </div>
    </section>
    <!--================End Category Product Area =================-->
@endsection