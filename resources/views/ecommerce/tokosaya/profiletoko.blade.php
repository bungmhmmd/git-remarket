@extends('layouts.ecommerce')

@section('title')
    <title>Proflie Toko - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
		<div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Proflie Toko</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.orders') }}">Proflie Toko</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>
						@if($user->status_toko == 0)
							<div class="col-md-9">
								@if (session('success'))
									<div class="alert alert-success">{{ session('success') }}</div>
								@endif

								@if (session('error'))
									<div class="alert alert-danger">{{ session('error') }}</div>
								@endif


								<div class="card text-dark">
									<div class="card-header">
										<h4 class="card-title">aktifkan toko</h4>
									</div>
									<div class="card-body">
										<form action="{{ route('customer.aktifkantoko') }}" method="post">
											@csrf
											<div class="form-group">
												<label for="">username toko</label>
												<input type="text" name="username_toko" class="form-control" required>
												<p class="text-muted text-sm-left">*masukan username tanpa spasi, username toko tidak dapat diganti!</p>
												<p class="text-danger">{{ $errors->first('username_toko') }}</p>
											</div>
											<div class="form-group">
												<label for="">nama toko</label>
												<input type="text" name="nama_toko" class="form-control" required>
												<p class="text-danger">{{ $errors->first('nama_toko') }}</p>
											</div>

											<button class="btn btn-primary btn-sm">Simpan</button>
										</form>
									</div>
								</div>
							</div>
						@else
						<div class="col-md-9">
							<div class="row">
								<div class="col-lg-12">
									<ul class="nav nav-tabs" style="font-size: 25px">
										<li class="show active" style="padding: 10px"><a  data-toggle="tab" href="#home">Profile Toko</a></li>
										<li style="padding: 10px"><a  data-toggle="tab" href="#menu1" >Tambah Bank</a></li>
									</ul>
								</div>
								<div class="col-lg-12">
									<div class="tab-content">
										<div id="home" class="tab-pane fade show active">
											<div class="card text-dark">
												<div class="card-header">

													<div class="row">
														<div class="col-lg-12">
																<form class="float-right" method="post" id="uploadbanner" enctype="multipart/form-data" >
																	<div class="" >
																		{!! csrf_field() !!}
																		<input type="file" id="inpuploadbanner" name="uploadbanner" style="display:none" />
																		<input type="button" class="btn btn-sm btn-outline-primary" id="btnuploadbanner" value="Upload Banner" />
																		<button type="reset" id="hapusbanner" onclick="hapusbanners()"  class="btn btn-sm btn-outline-danger">Hapus Banner</button>

																	</div>
																</form>
																<br>
																<br>
															<div class="card-profile-image" style="margin-bottom: 40px">
																@if($profiletoko->banner == null)
																	<img src="{{asset('tokosaya/2.png')}}" alt="image" id="banner" class=""
																		 style="
                                      width: 100%;
                                         min-height: 350px;
  										max-aspect-ratio: 1/1;
  										 object-fit: cover;

                                        ">
																@else
																	<img src="{{asset('/tokosaya/'.$profiletoko->username_toko).'/'.$profiletoko->banner}}" alt="image" id="banner"  class=""
																		 style="
                                           width: 100%;
                                           max-height: 300px;
  										max-aspect-ratio: 1/1;
                                       object-fit: cover;
                                        ">
																@endif														
															</div>
														</div>
													</div>

													<div class="row" style="margin-top: -200px;margin-left: 50px">
														<div class="col-lg-12 order-lg-12">
															<div class="card-profile-image" style="margin-bottom: 40px">
																@if($profiletoko->avatar == null)
																	<img src="{{asset('tokosaya/1.jpg')}}" alt="image" id="avatar" class="border border-white bg-white card-img-top img-fluid "
																		 style="
                                        width: 225px;
                                        height: 225px;
                                        max-width:225px;
                                        max-height:225px;
                                        object-fit:scale-down;
                                        border-radius:50% 50% 50% 50%;
                                        ">
																@else
																	<img src="{{asset('/tokosaya/'.$profiletoko->username_toko).'/'.$profiletoko->avatar}}" alt="image" id="avatar"  class="border border-white bg-white card-img-top img-fluid "
																		 style="
                                        width: 225px;
                                        height: 225px;
                                        max-width:225px;
                                        max-height:225px;
                                        object-fit:scale-down;
                                        border-radius:50% 50% 50% 50%;
                                        ">
																@endif

																		<form method="post" id="uploadavatar" enctype="multipart/form-data">
																			<div class="" style="margin-top: -60px;margin-left: 220px">
																				{!! csrf_field() !!}
																				<input type="file" id="inpupload" name="uploadava" style="display:none" />
																				<input type="button" class="btn btn-sm btn-outline-primary" id="btnupload" value="Upload" />
																				<button type="reset" id="hapusavatar" onclick="hapusava()"  class="btn btn-sm btn-outline-danger">Hapus</button>
																			</div>
																		</form>
															</div>
														</div>
													</div>

													<h4 class="card-title">Informasi Toko</h4>
												</div>
												<div class="card-body">
													@if (session('success'))
														<div class="alert alert-success">{{ session('success') }}</div>
													@endif

													<form action="{{ route('customer.profiletoko.update') }}" method="post">
														@csrf
														<div class="form-group">
															<label for="">Nama Lengkap</label>
															<input type="text" name="nama_toko" class="form-control" required value="{{ $profiletoko->nama_toko }}">
															<p class="text-danger">{{ $errors->first('nama_toko') }}</p>
														</div>
														<div class="form-group">
															<label for="">username toko</label>
															<input type="text" name="username_toko" class="form-control" required value="{{ $profiletoko->username_toko }}" readonly>
															<p class="text-danger">{{ $errors->first('username_toko') }}</p>
														</div>
														<div class="form-group">
															<label for="">No Telp</label>
															<input type="text" name="phone_number" class="form-control" required value="{{ $profiletoko->phone_number }}">
															<p class="text-danger">{{ $errors->first('phone_number') }}</p>
														</div>
														<div class="form-group">
															<label for="">Alamat</label>
															<input type="text" name="address" class="form-control" required value="{{ $profiletoko->address }}">
															<p class="text-danger">{{ $errors->first('address') }}</p>
														</div>
                                                        <div class="form-group">
                                                            <label for="">Deskripsi Toko</label>
                                                            <textarea type="text" name="deskripsi_toko" class="form-control" required >{{ $profiletoko->deskripsi_toko }}</textarea>
                                                            <p class="text-danger">{{ $errors->first('address') }}</p>
                                                        </div>
														<div class="form-group">
															<label for="">Propinsi</label>
															<select class="form-control" name="province_id" id="province_id" required>
																<option value="">Pilih Propinsi</option>
																@if(!empty($customer->district))
																	@foreach ($provinces as $row)
																		<option value="{{ $row->id }}" {{ $customer->district->province_id == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
																	@endforeach
																@else
																@foreach ($provinces as $row)
																	<option value="{{ $row->id }}">{{ $row->name }}</option>
																@endforeach
																@endif
															</select>
															<p class="text-danger">{{ $errors->first('province_id') }}</p>
														</div>
														<div class="form-group">
															<label for="">Kabupaten / Kota</label>
															<select class="form-control" name="city_id" id="city_id" required>
																<option value="">Pilih Kabupaten/Kota</option>
															</select>
															<p class="text-danger">{{ $errors->first('city_id') }}</p>
														</div>
														<div class="form-group">
															<label for="">Kecamatan</label>
															<select class="form-control" name="district_id" id="district_id" required>
																<option value="">Pilih Kecamatan</option>
															</select>
															<p class="text-danger">{{ $errors->first('district_id') }}</p>
														</div>
														<button class="btn btn-primary btn-sm">Simpan</button>	
														<a class="btn-success btn-sm" href="{{ url('/toko/' . $profiletoko->username_toko.'/all') }}">Lihat profil toko</a>													
													</form>
												</div>
											</div>
										</div>
										<div id="menu1" class="tab-pane fade show">

											<div class="card text-dark">
												<div class="card-header">
													<h4 class="card-title">
														List Rekening Bank Toko
													</h4>
												</div>
												<div class="card-body">
													@if (session('success'))
														<div class="alert alert-success">{{ session('success') }}</div>
													@endif

													@if (session('error'))
														<div class="alert alert-danger">{{ session('error') }}</div>
													@endif

														<a href="{{ route('customer.createbank') }}" class="btn btn-primary btn-sm">Tambah</a>
														<form action="{{ route('customer.profiletoko') }}" method="get">
															<div class="input-group mb-3 col-md-3 float-right">
																<input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
																<div class="input-group-append">
																	<button class="btn btn-secondary" type="button">Cari</button>
																</div>
															</div>
														</form>
													<div class="table-responsive">
														<table class="table table-hover table-bordered">
															<thead>
															<tr>
																<th>Nomor Rekening</th>
																<th>Atas Nama</th>
																<th>Bank</th>
																<th>Tanggal</th>
																<th>Aksi</th>
															</tr>
															</thead>
															<tbody>
															@forelse ($bank as $row)
																<tr>
																	<td><strong>{{ $row->no_rek }}</strong></td>
																	<td>
																		<strong>{{ $row->atasnama }}</strong><br>
																	</td>
																	<td>
																		<strong>{{ $row->bank }}</strong><br>
																	</td>
																	<td>{{ $row->created_at->format('d-m-Y') }}</td>
																	<td>
																		<form action="{{ route('customer.hapusbank') }}" method="post">
																			@csrf
																			<input name="id" type="hidden" value="{{$row->id}}">
																			<a href="{{ route('customer.editbank', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
																			<button class="btn btn-danger btn-sm">Hapus</button>
																		</form>
																	</td>
																</tr>
															@empty
																<tr>
																	<td colspan="6" class="text-center">Tidak ada data</td>
																</tr>
															@endforelse
															</tbody>
														</table>
													</div>
												</div>
											</div>

										</div>
									</div>

								</div>
							</div>

						</div>

						</div>
							@endif
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection

@section('js')
	<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('deskripsi_toko');
    </script>
	<script>
        $(document).ready(function(){
            loadCity($('#province_id').val(), 'bySelect').then(() => {
                loadDistrict($('#city_id').val(), 'bySelect');
            })
        })

        $('#province_id').on('change', function() {
            loadCity($(this).val(), '');
        })

        $('#city_id').on('change', function() {
            loadDistrict($(this).val(), '')
        })

        function loadCity(province_id, type) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: "{{ url('/api/city') }}",
                    type: "GET",
                    data: { province_id: province_id },
                    success: function(html){
                        $('#city_id').empty()
                        $('#city_id').append('<option value="">Pilih Kabupaten/Kota</option>')
                        $.each(html.data, function(key, item) {
									@if($profiletoko->district_id !== 0)
                            let city_selected = {{ $customer->district->city_id }};
									@else
                            let city_selected = "";
									@endif
                            let selected = type == 'bySelect' && city_selected == item.id ? 'selected':'';
                            $('#city_id').append('<option value="'+item.id+'" '+ selected +'>'+item.name+'</option>')
                            resolve()
                        })
                    }
                });
            })
        }

        function loadDistrict(city_id, type) {
            $.ajax({
                url: "{{ url('/api/district') }}",
                type: "GET",
                data: { city_id: city_id },
                success: function(html){
                    $('#district_id').empty()
                    $('#district_id').append('<option value="">Pilih Kecamatan</option>')
                    $.each(html.data, function(key, item) {
								@if($profiletoko->district_id !== 0)
                        let district_selected = {{ $customer->district->id }};
								@else
                        let district_selected = "";
								@endif
                        let selected = type == 'bySelect' && district_selected == item.id ? 'selected':'';
                        $('#district_id').append('<option value="'+item.id+'" '+ selected +'>'+item.name+'</option>')
                    })
                }
            });
        }

        function hapusava() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			var username = "{{$profiletoko->username_toko}}";
                    $.ajax({
                        type  : 'POST',
                        url   : "{{ route('customer.profiletoko.hapusava') }}",
                        dataType : 'JSON',
                        data : {username: username},
                        cache:false,
                        success : function(){
                                    document.getElementById("avatar").src = "{{asset('tokosaya/1.jpg')}}";
                                    document.getElementById("hapusavatar").style.display = "none";

                        }
                    }); //akhir ajax
        }

        $(document.body).ready(function(){
            $("#btnupload").click(function(){ $("#inpupload").trigger("click"); });
            $("#inpupload").change(function(){ $("#uploadavatar").submit(); });

            var adaava = "{{$profiletoko->avatar}}";
            if (adaava == "") {
                document.getElementById("hapusavatar").style.display = "none";
            }
        });

        $('#uploadavatar').on('submit', function(event){
            //var warna =  $('.batchCheckbox:checked').serialize().replace(/%5B%5D/g, '[]');
            event.preventDefault();

            $.ajax({
                method:"post",
                url: "{{ route('customer.profiletoko.uploudava') }}",
                data:  new FormData(this),
                processData: false,
                contentType: false,
                success: function(data){
                                document.getElementById("avatar").src = "{{asset('/tokosaya/'.$user->username_toko).'/'.$user->avatar}}/"+data;
                                document.getElementById("hapusavatar").style.display = "inline-block";
                },
                error: function () {

                }
            });



        });

        function hapusbanners() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var username = "{{$profiletoko->username_toko}}";
            $.ajax({
                type  : 'POST',
                url   : "{{ route('customer.profiletoko.hapusbanner') }}",
                dataType : 'JSON',
                data : {username: username},
                cache:false,
                success : function(){
                    document.getElementById("banner").src = "{{asset('tokosaya/2.png')}}";
                    document.getElementById("hapusbanner").style.display = "none";

                }
            }); //akhir ajax
        }

        $(document.body).ready(function(){
            $("#btnuploadbanner").click(function(){ $("#inpuploadbanner").trigger("click"); });
            $("#inpuploadbanner").change(function(){ $("#uploadbanner").submit(); });

            var adabanner = "{{$profiletoko->banner}}";
            if (adabanner == "") {
                document.getElementById("hapusbanner").style.display = "none";
            }
        });

        $('#uploadbanner').on('submit', function(event){
            //var warna =  $('.batchCheckbox:checked').serialize().replace(/%5B%5D/g, '[]');
            event.preventDefault();

            $.ajax({
                method:"post",
                url: "{{ route('customer.profiletoko.uploudbanner') }}",
                data:  new FormData(this),
                processData: false,
                contentType: false,
                success: function(data){
                    document.getElementById("banner").src = "{{asset('/tokosaya/'.$user->username_toko).'/'.$user->banner}}/"+data;
                    document.getElementById("hapusbanner").style.display = "inline-block";
                },
                error: function () {

                }
            });



        });
	</script>
@endsection