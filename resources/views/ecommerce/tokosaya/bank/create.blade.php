@extends('layouts.ecommerce')

@section('title')
    <title>Tambah Rekening - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
			<div class="overlay"></div>
				<div class="banner_content text-center">
					<h2>Tambah Rekening</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.orders') }}">Tambah Rekening</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>

						<div class="col-md-9">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('customer.storebank') }}" method="post" enctype="multipart/form-data" >
										@csrf
										<div class="row">
											<div class="col-md-8">
												<div class="card">
													<div class="card-header">
														<h4 class="card-title">Tambah Rekening</h4>
													</div>
													<div class="card-body">
														<div class="form-group">
															<label for="name">Nama Pemilik Rekening</label>
															<input type="text" name="pemilik_rek" class="form-control" value="{{ old('pemilik_rek') }}" required>
															<p class="text-danger">{{ $errors->first('pemilik_rek') }}</p>
														</div>
														<div class="form-group">
															<label for="name">Nomor Rekening</label>
															<input type="text" name="no_rek" class="form-control" value="{{ old('no_rek') }}" required>
															<p class="text-danger">{{ $errors->first('no_rek') }}</p>
														</div>
														<div class="form-group">
															<label for="name">Bank</label>
															<input type="text" name="bank" class="form-control" value="{{ old('bank') }}" required>
															<p class="text-danger">{{ $errors->first('bank') }}</p>
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-body">
														<div class="form-group">
															<button class="btn btn-primary btn-sm">Tambah</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection