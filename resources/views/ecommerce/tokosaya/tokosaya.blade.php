@extends('layouts.ecommerce')

@section('title')
    <title>Toko Saya - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
			<div class="overlay"></div>
				<div class="banner_content text-center">
					<h2>Toko Saya</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.orders') }}">Toko Saya</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>
						@if($user->status_toko == 0)
							<div class="col-md-9">
								@if (session('success'))
									<div class="alert alert-success">{{ session('success') }}</div>
								@endif

								@if (session('error'))
									<div class="alert alert-danger">{{ session('error') }}</div>
								@endif


								<div class="card">
									<div class="card-header">
										<h4 class="card-title">aktifkan toko</h4>
									</div>
									<div class="card-body">
										<form action="{{ route('customer.aktifkantoko') }}" method="post">
											@csrf
											<div class="form-group">
												<label for="">username toko</label>
												<input type="text" name="username_toko" class="form-control" required>
												<p class="text-muted text-sm-left">*masukan username tanpa spasi, username toko tidak dapat diganti!</p>
												<p class="text-danger">{{ $errors->first('username_toko') }}</p>
											</div>
											<div class="form-group">
												<label for="">nama toko</label>
												<input type="text" name="nama_toko" class="form-control" required>
												<p class="text-danger">{{ $errors->first('nama_toko') }}</p>
											</div>
											<div class="form-group">
												<label for="">deskripsi toko</label>
												<textarea name="deskripsi_toko" class="form-control" required></textarea>
											</div>
											<button class="btn btn-primary btn-sm">Simpan</button>
										</form>
									</div>
								</div>
							</div>
						@elseif($user->status_toko == 2)
								<div class="col-md-9">
										<div class="alert alert-warning">harap menunggu konfirmasi toko anda</div>
									<div class="card">
										<div class="card-body">


												<div class="form-group">
													<label for="">username toko</label>
													<input type="text" class="form-control" disabled value="{{$profiletoko->username_toko}}">
												</div>
												<div class="form-group">
													<label for="">nama toko</label>
													<input type="text" class="form-control" disabled value="{{$profiletoko->nama_toko}}">
												</div>
											<div class="form-group">
												<label for="">deskripsi toko</label>
												<input type="text" class="form-control" disabled value="{{$profiletoko->deskripsi_toko}}">
											</div>
										</div>
									</div>
								</div>
							@else
						<div class="col-md-9">
							@if (session('success'))
								<div class="alert alert-success">{{ session('success') }}</div>
							@endif

							@if (session('error'))
								<div class="alert alert-danger">{{ session('error') }}</div>
							@endif
							<div class="row">
								<div class="col-lg-12">
									<ul class="nav nav-tabs" style="font-size: 25px">
										<li class="show active" style="padding: 10px"><a  data-toggle="tab" href="#home">List Produk</a></li>
										<li style="padding: 10px"><a  data-toggle="tab" href="#menu1" >Pesanan</a></li>
										<li style="padding: 10px"><a  data-toggle="tab" href="#menu2">Laporan Order</a></li>
										<li style="padding: 10px"><a  data-toggle="tab" href="#menu3">Laporan Return</a></li>
									</ul>
								</div>
								<div class="col-lg-12">
									<div class="tab-content">
										<div id="home" class="tab-pane fade show active">

											<div class="card">
												<div class="card-body text-dark">
                                                    <a href="{{ route('customer.createproduk') }}" class="btn btn-primary btn-sm">Tambah</a>
													<form action="{{ route('customer.tokosaya') }}" method="get">
														<div class="input-group mb-3 col-md-3 float-right">
															<input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
															<div class="input-group-append">
																<button class="btn btn-secondary" type="button">Cari</button>
															</div>
														</div>
													</form>
													<div class="table-responsive">
														<table class="table table-hover table-bordered">
															<thead>
															<tr>
																<th>#</th>
																<th>Produk</th>
																<th>Harga</th>
																<th>Created At</th>
																<th>Status</th>
																<th>Aksi</th>
															</tr>
															</thead>
															<tbody>
															@forelse ($product as $row)
																<tr>
																	<td>
																		<img src="{{ asset('storage/products/' . $row->image) }}" width="100px" height="100px" alt="{{ $row->name }}">
																	</td>
																	<td>
																		<strong>{{ $row->name }}</strong><br>
																		<label>Kategori: <span class="badge badge-info">{{ $row->category->name }}</span></label><br>
																		<label>Berat: <span class="badge badge-info">{{ $row->weight }} kg</span></label>
																	</td>
																	<td>Rp {{ number_format($row->price) }}</td>
																	<td>{{ $row->created_at->format('d-m-Y') }}</td>
																	<td>{!! $row->status_label !!}</td>
																	<td>
																		<form action="{{ route('customer.hapusproduk') }}" method="post">
																			@csrf
																			<input name="id" type="hidden" value="{{$row->id}}">
																			<a href="{{ route('customer.editproduk', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
																			<button class="btn btn-danger btn-sm">Hapus</button>
																		</form>
																	</td>
																</tr>
															@empty
																<tr>
																	<td colspan="6" class="text-center">Tidak ada data</td>
																</tr>
															@endforelse
															</tbody>
														</table>
													</div>
													{!! $product->links() !!}
												</div>
											</div>

										</div>
										<div id="menu1" class="tab-pane fade show">

											<div class="card text-dark">
												<div class="card-header">
													<h4 class="card-title">
														Daftar Pesanan
													</h4>
												</div>
												<div class="card-body">


													<form action="{{ route('customer.tokosaya') }}" method="get">
														<div class="input-group mb-3 col-md-6 float-right">
															<select name="status" class="form-control mr-3">
																<option value="">Pilih Status</option>
																<option value="0">Baru</option>
																<option value="1">Confirm</option>
																<option value="2">Proses</option>
																<option value="3">Dikirim</option>
																<option value="4">Selesai</option>
															</select>
															<input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
															<div class="input-group-append">
																<button class="btn btn-secondary" type="submit">Cari</button>
															</div>
														</div>
													</form>
													<div class="table-responsive">
														<table class="table table-hover table-bordered">
															<thead>
															<tr>
																<th>InvoiceID</th>
																<th>Pelanggan</th>
																<th>Subtotal</th>
																<th>Tanggal</th>
																<th>Status</th>
																<th>Aksi</th>
															</tr>
															</thead>
															<tbody>
															@forelse ($orders as $row)
																<tr>
																	<td><strong>{{ $row->invoice }}</strong></td>
																	<td>
																		<strong>{{ $row->customer_name }}</strong><br>
																		<label><strong>Telp:</strong> {{ $row->customer_phone }}</label><br>
																		<label><strong>Alamat:</strong> {{ $row->customer_address }} {{ $row->customer->district->name }} - {{  $row->customer->district->city->name}}, {{ $row->customer->district->city->province->name }}</label>
																	</td>
																	<td>Rp {{ number_format($row->subtotal) }}</td>
																	<td>{{ $row->created_at->format('d-m-Y') }}</td>
																	<td>
																		{!! $row->status_label !!} <br>
																		@if ($row->return_count > 0)
																			<a href="{{ route('orderstoko.return', $row->invoice) }}">Permintaan Return</a>
																		@endif
																	</td>
																	<td>
																		<form action="{{ route('orderstoko.destroy', $row->id) }}" method="post">
																			@csrf
																			@method('DELETE')
																			<a href="{{ route('orderstoko.view', $row->invoice) }}" class="btn btn-warning btn-sm">Lihat</a>
																			<button class="btn btn-danger btn-sm">Hapus</button>
																		</form>
																	</td>
																</tr>
															@empty
																<tr>
																	<td colspan="6" class="text-center">Tidak ada data</td>
																</tr>
															@endforelse
															</tbody>
														</table>
													</div>
													{!! $orders->links() !!}
												</div>
											</div>

										</div>
										<div id="menu2" class="tab-pane fade show">

											<div class="card text-dark">
												<div class="card-header">
													<h4 class="card-title">
														Laporan Order
													</h4>
												</div>
												<div class="card-body">

													<form action="{{ route('customer.tokosaya') }}" method="get">
														<div class="input-group mb-3 col-md-4 float-right">
															<input type="text" id="created_at_order" name="date" class="form-control">
															<div class="input-group-append">
																<button class="btn btn-secondary" type="submit">Filter</button>
															</div>
															<a target="_blank" class="btn btn-primary ml-2" id="exportpdf_order">Export PDF</a>
														</div>
													</form>
													<div class="table-responsive">
														<table class="table table-hover table-bordered">
															<thead>
															<tr>
																<th>InvoiceID</th>
																<th>Pelanggan</th>
																<th>Subtotal</th>
																<th>Tanggal</th>
															</tr>
															</thead>
															<tbody>
															@forelse ($laporanorders as $row)
																<tr>
																	<td><strong>{{ $row->invoice }}</strong></td>
																	<td>
																		<strong>{{ $row->customer_name }}</strong><br>
																		<label><strong>Telp:</strong> {{ $row->customer_phone }}</label><br>
																		<label><strong>Alamat:</strong> {{ $row->customer_address }} {{ $row->customer->district->name }} - {{  $row->customer->district->city->name}}, {{ $row->customer->district->city->province->name }}</label>
																	</td>
																	<td>Rp {{ number_format($row->subtotal) }}</td>
																	<td>{{ $row->created_at->format('d-m-Y') }}</td>
																</tr>
															@empty
																<tr>
																	<td colspan="6" class="text-center">Tidak ada data</td>
																</tr>
															@endforelse
															</tbody>
														</table>
													</div>
												</div>
											</div>

										</div>
										<div id="menu3" class="tab-pane fade show">
											<div class="card text-dark">
												<div class="card-header">
													<h4 class="card-title">
														Laporan Return
													</h4>
												</div>
												<div class="card-body">


													<form action="{{ route('customer.tokosaya') }}" method="get">
														<div class="input-group mb-3 col-md-4 float-right">
															<input type="text" id="created_at_return" name="date" class="form-control">
															<div class="input-group-append">
																<button class="btn btn-secondary" type="submit">Filter</button>
															</div>
															<a target="_blank" class="btn btn-primary ml-2" id="exportpdf_return">Export PDF</a>
														</div>
													</form>
													<div class="table-responsive">
														<table class="table table-hover table-bordered">
															<thead>
															<tr>
																<th>InvoiceID</th>
																<th>Pelanggan</th>
																<th>Subtotal</th>
																<th>Tanggal</th>
															</tr>
															</thead>
															<tbody>
															@forelse ($laporanreturn as $row)
																<tr>
																	<td><strong>{{ $row->invoice }}</strong></td>
																	<td>
																		<strong>{{ $row->customer_name }}</strong><br>
																		<label><strong>Telp:</strong> {{ $row->customer_phone }}</label><br>
																		<label><strong>Alamat:</strong> {{ $row->customer_address }} {{ $row->customer->district->name }} - {{  $row->customer->district->city->name}}, {{ $row->customer->district->city->province->name }}</label>
																	</td>
																	<td>Rp {{ number_format($row->subtotal) }}</td>
																	<td>{{ $row->created_at->format('d-m-Y') }}</td>
																</tr>
															@empty
																<tr>
																	<td colspan="6" class="text-center">Tidak ada data</td>
																</tr>
															@endforelse
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>

						</div>
							@endif
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection