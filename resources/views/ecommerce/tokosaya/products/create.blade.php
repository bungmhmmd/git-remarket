@extends('layouts.ecommerce')

@section('title')
    <title>Tambah Produk - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
			<div class="overlay"></div>
				<div class="banner_content text-center">
					<h2>Tambah Produk</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.orders') }}">Tambah Produk</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>

						<div class="col-md-9">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('customer.storeproduk') }}" method="post" enctype="multipart/form-data" >
										@csrf
										<div class="row">
											<div class="col-md-8">
												<div class="card">
													<div class="card-header">
														<h4 class="card-title">Tambah Produk</h4>
													</div>
													<div class="card-body">
														<div class="form-group">
															<label for="name">Nama Produk</label>
															<input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
															<p class="text-danger">{{ $errors->first('name') }}</p>
														</div>
														<div class="form-group">
															<label for="description">Deskripsi</label>
															<textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
															<p class="text-danger">{{ $errors->first('description') }}</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="card">
													<div class="card-body">
														<div class="form-group">
															<label for="status">Status</label>
															<select name="status" class="form-control" required>
																<option value="1" {{ old('status') == '1' ? 'selected':'' }}>Publish</option>
																<option value="0" {{ old('status') == '0' ? 'selected':'' }}>Draft</option>
															</select>
															<p class="text-danger">{{ $errors->first('status') }}</p>
														</div>
														<div class="form-group">
															<label for="category_id">Kategori</label>
															<select name="category_id" class="form-control">
																<option value="">Pilih</option>
																@foreach ($category as $row)
																	<option value="{{ $row->id }}" {{ old('category_id') == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
																@endforeach
															</select>
															<p class="text-danger">{{ $errors->first('category_id') }}</p>
														</div>
														<div class="form-group">
															<label for="price">Harga</label>
															<input type="number" name="price" class="form-control" value="{{ old('price') }}" required>
															<p class="text-danger">{{ $errors->first('price') }}</p>
														</div>
														<div class="form-group">
															<label for="weight">Berat</label>
															<input type="number" name="weight" class="form-control" value="{{ old('weight') }}" required>
															<p class="text-danger">{{ $errors->first('weight') }}</p>
														</div>
														<div class="form-group">
															<label for="image">Foto Produk</label>
															<input type="file" name="image" class="form-control" value="{{ old('image') }}" required>
															<p class="text-danger">{{ $errors->first('image') }}</p>
														</div>
														<div class="form-group">
															<button class="btn btn-primary btn-sm">Tambah</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection