@extends('layouts.ecommerce')

@section('title')
    <title>Edit Produk - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
			<div class="overlay"></div>
				<div class="banner_content text-center">
					<h2>Edit Produk</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.orders') }}">Edit Produk</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>

						<div class="col-md-9">
							<div class="card">
								<div class="card-body">
									<form action="{{ route('customer.updateproduk', $product->id) }}" method="post" enctype="multipart/form-data" >
										@csrf
										@method('PUT')

										<div class="row">
											<div class="col-md-8">
												<div class="card">
													<div class="card-header">
														<h4 class="card-title">Edit Produk</h4>
													</div>
													<div class="card-body">
														<div class="form-group">
															<label for="name">Nama Produk</label>
															<input type="text" name="name" class="form-control" value="{{ $product->name }}" required>
															<p class="text-danger">{{ $errors->first('name') }}</p>
														</div>
														<div class="form-group">
															<label for="description">Deskripsi</label>
															<textarea name="description" id="description" class="form-control">{{ $product->description }}</textarea>
															<p class="text-danger">{{ $errors->first('description') }}</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="card">
													<div class="card-body">
														<div class="form-group">
															<label for="status">Status</label>
															<select name="status" class="form-control" required>
																<option value="1" {{ $product->status == '1' ? 'selected':'' }}>Publish</option>
																<option value="0" {{ $product->status == '0' ? 'selected':'' }}>Draft</option>
															</select>
															<p class="text-danger">{{ $errors->first('status') }}</p>
														</div>
														<div class="form-group">
															<label for="category_id">Kategori</label>
															<select name="category_id" class="form-control">
																<option value="">Pilih</option>
																@foreach ($category as $row)
																	<option value="{{ $row->id }}" {{ $product->category_id == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
																@endforeach
															</select>
															<p class="text-danger">{{ $errors->first('category_id') }}</p>
														</div>
														<div class="form-group">
															<label for="price">Harga</label>
															<input type="number" name="price" class="form-control" value="{{ $product->price }}" required>
															<p class="text-danger">{{ $errors->first('price') }}</p>
														</div>
														<div class="form-group">
															<label for="weight">Berat</label>
															<input type="number" name="weight" class="form-control" value="{{ $product->weight }}" required>
															<p class="text-danger">{{ $errors->first('weight') }}</p>
														</div>
														<div class="form-group">
															<label for="image">Foto Produk</label>
															<br>
															<img src="{{ asset('storage/products/' . $product->image) }}" width="100px" height="100px" alt="{{ $product->name }}">
															<hr>
															<input type="file" name="image" class="form-control">
															<p><strong>Biarkan kosong jika tidak ingin mengganti gambar</strong></p>
															<p class="text-danger">{{ $errors->first('image') }}</p>
														</div>
														<div class="form-group">
															<button class="btn btn-primary btn-sm">Update</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection