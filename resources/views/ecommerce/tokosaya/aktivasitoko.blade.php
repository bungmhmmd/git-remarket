@extends('layouts.ecommerce')

@section('title')
    <title>Aktivasi Toko - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Aktivasi Toko</h2>
					<div class="page_link">
						<a href="{{ url('/') }}">Home</a>
						<a href="{{ route('customer.aktivasitoko') }}">Aktivasi toko</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-md-3">
							@include('layouts.ecommerce.module.sidebar')
						</div>
						@if($user->status_toko == 0)
                        <div class="col-md-9">
								@if (session('success'))
									<div class="alert alert-success">{{ session('success') }}</div>
								@endif

								@if (session('error'))
									<div class="alert alert-danger">{{ session('error') }}</div>
								@endif
									<form action="{{ route('customer.kirimaktivasi') }}" method="post" enctype="multipart/form-data" >
									@csrf
									<div class="row">
										<div class="col-md-8">
											<div class="card">
												<div class="card-header">
													<h4 class="card-title">Aktivasi Toko</h4>
												</div>
												<div class="card-body">
													<div class="form-group">
														<label for="name">Nama Toko</label>
														<input type="text" name="toko_name" class="form-control" value="{{ old('toko_name') }}" required>
														<p class="text-danger">{{ $errors->first('toko_name') }}</p>
													</div>
													<div class="form-group">
														<label for="description">Deskripsi</label>
														<p class="text-muted text-sm-left">*Deskripsi toko berisi penjelasan dari toko yang anda miliki dan penjelasan dari produk akan di jual. </p>
														<p class="text-muted text-sm-left">**Re:market hanya menerima toko yang menjual produk dari hasil olahan bahan daur ulang atau sampah. </p>
														<textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
														<p class="text-danger">{{ $errors->first('description') }}</p>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card">
												<div class="card-body">																													
													<div class="form-group">
														<label for="toko_username">Username Toko</label>
														<input type="text" name="toko_username" class="form-control" value="{{ old('toko_username') }}" required>
														<p class="text-muted text-sm-left">*masukan username tanpa spasi, username toko tidak dapat diganti!</p>
														<p class="text-danger">{{ $errors->first('toko_username') }}</p>
													</div>
													<div class="form-group">
														<label for="image">Foto Salah Satu Produk</label>
														<input type="file" name="image" class="form-control" value="{{ old('image') }}" required>
														<p class="text-danger">{{ $errors->first('image') }}</p>
													</div>
													<input type="hidden" name="customer_id" value="{{ auth()->guard('customer')->user()->id }}" class="form-control">
													<input type="hidden" name="customer_name" value="{{ auth()->guard('customer')->user()->name }}" class="form-control">
													<div class="form-group">
														<button class="btn btn-primary btn-sm">Kirim Permintaan Aktivasi Toko</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						@elseif($user->status_toko == 2)
							<div class="col-md-9">
								<div class="alert alert-warning">harap menunggu konfirmasi toko anda</div>
                                
								@if (session('success'))
									<div class="alert alert-success">{{ session('success') }}</div>
								@endif

								@if (session('error'))
									<div class="alert alert-danger">{{ session('error') }}</div>
								@endif

									<form action="{{ route('customer.aktivasitoko') }}" method="post" enctype="multipart/form-data" >
									@csrf
									<div class="row">
										<div class="col-md-8">
											<div class="card">
												<div class="card-header">
													<h4 class="card-title">Aktivasi Toko</h4>
												</div>
												<div class="card-body">
													<div class="form-group">
														<label for="name">Nama Toko</label>
														<input type="text" name="toko_name" class="form-control" value="{{ $aktivasitoko->toko_name }}" required>
														<p class="text-danger">{{ $errors->first('toko_name') }}</p>
													</div>
													<div class="form-group">
														<label for="description">Deskripsi</label>			
														<textarea name="description" id="description" class="form-control">{!! $aktivasitoko->description !!}</textarea>
														<p class="text-danger">{{ $errors->first('description') }}</p>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card">
												<div class="card-body">																													
													<div class="form-group">
														<label for="toko_username">Username Toko</label>
														<input type="text" name="toko_username" class="form-control" value="{{ $aktivasitoko->toko_username }}" required>
														<p class="text-muted text-sm-left">*masukan username tanpa spasi, username toko tidak dapat diganti!</p>
														<p class="text-danger">{{ $errors->first('toko_username') }}</p>
													</div>
                                                    <br>
                                                    <br>
                                                    <img src="{{ asset('storage/aktivasi_toko/' . $aktivasitoko->image) }}" width="200px" height="200px" alt="{{ $aktivasitoko->toko_name }}">
													<div class="form-group">
														<label for="image">Foto Salah Satu Produk</label>
                                                        
														<input type="file" name="image" class="form-control" value="{{ old('image') }}" required>
														<p class="text-danger">{{ $errors->first('image') }}</p>
													</div>
													<input type="hidden" name="customer_id" value="{{ auth()->guard('customer')->user()->id }}" class="form-control">
													<input type="hidden" name="customer_name" value="{{ auth()->guard('customer')->user()->name }}" class="form-control">
													<div class="form-group">
														
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
                            @elseif($user->status_toko == 3)
							<div class="col-md-9">
								<div class="alert alert-danger">Permintaan aktivasi anda kami tolak karena toko anda tidak sesuai dengan Re:market</div>
                                
								@if (session('success'))
									<div class="alert alert-success">{{ session('success') }}</div>
								@endif

								@if (session('error'))
									<div class="alert alert-danger">{{ session('error') }}</div>
								@endif

									<form action="{{ route('customer.kirimaktivasi') }}" method="post" enctype="multipart/form-data" >
									@csrf
									<div class="row">
										<div class="col-md-8">
											<div class="card">
												<div class="card-header">
													<h4 class="card-title">Aktivasi Toko</h4>
												</div>
												<div class="card-body">
													<div class="form-group">
														<label for="name">Nama Toko</label>
														<input type="text" name="toko_name" class="form-control" value="{{ $aktivasitoko->toko_name }}" required>
														<p class="text-danger">{{ $errors->first('toko_name') }}</p>
													</div>
													<div class="form-group">
														<label for="description">Deskripsi</label>			
														<textarea name="description" id="description" class="form-control">{!! $aktivasitoko->description !!}</textarea>
														<p class="text-danger">{{ $errors->first('description') }}</p>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card">
												<div class="card-body">																													
													<div class="form-group">
														<label for="toko_username">Username Toko</label>
														<input type="text" name="toko_username" class="form-control" value="{{ $aktivasitoko->toko_username }}" required>
														<p class="text-muted text-sm-left">*masukan username tanpa spasi, username toko tidak dapat diganti!</p>
														<p class="text-danger">{{ $errors->first('toko_username') }}</p>
													</div>
                                                    <br>
                                                    <br>
                                                    <img src="{{ asset('storage/aktivasi_toko/' . $aktivasitoko->image) }}" width="200px" height="200px" alt="{{ $aktivasitoko->toko_name }}">
													<div class="form-group">
														<label for="image">Foto Salah Satu Produk</label>
                                                        
														<input type="file" name="image" class="form-control" value="{{ old('image') }}" required>
														<p class="text-danger">{{ $errors->first('image') }}</p>
													</div>
													<input type="hidden" name="customer_id" value="{{ auth()->guard('customer')->user()->id }}" class="form-control">
													<input type="hidden" name="customer_name" value="{{ auth()->guard('customer')->user()->name }}" class="form-control">
													<div class="form-group">
                                                    <button class="btn btn-primary btn-sm">Kirim Permintaan Aktivasi Toko</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							@endif
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->
@endsection

@section('js')
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
@endsection