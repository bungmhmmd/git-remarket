@extends('layouts.ecommerce')

@section('title')
    <title>Re:market - Marketplace Produk Recycle/Daur Ulang</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
		<section class="home_banner_area">
		<div class="overlay"></div>
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
				<div class="banner_content row">
					<div class="offset-lg-2 col-lg-8">
						<h3>
							<br />
							<br />Marketplace barang daur ulang pertama di Indonesia</h3>
							<a class="white_bg_btn" href="{{ route('front.product') }}">Belanja Sekarang</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!--================Hot Deals Area =================-->
	<section class="hot_deals_area section_gap">
				<div class="main_title text-center">
						<h2>Kategori Pilihan</h2>
				</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/product/hot_deals/kursi.jpg') }}" alt="">
						<div class="content">
							<h2>Kursi</h2>
							<p>Beli sekarang</p>
						</div>
						<a class="hot_deal_link" href="{{ route('front.category', 'kursi') }}"></a>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/product/hot_deals/tas.jpg') }}" alt="">
						<div class="content">
							<h2>Tas</h2>
							<p>Beli sekarang</p>
						</div>
						<a class="hot_deal_link" href="{{ route('front.category', 'tas') }}"></a>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/product/hot_deals/deal3.jpg') }}" alt="">
						<div class="content">
							<h2>SANDAL</h2>
							<p>Beli sekarang</p>
						</div>
						<a class="hot_deal_link" href="{{ route('front.category', 'sandal') }}"></a>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/product/hot_deals/deal4.jpg') }}" alt="">
						<div class="content">
							<h2>TOPI</h2>
							<p>Beli sekarang</p>
						</div>
						<a class="hot_deal_link" href="{{ route('front.category', 'topi') }}"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Hot Deals Area =================-->

	<!--================Feature Product Area =================-->
	<section class="feature_product_area section_gap">
		<div class="main_box">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main_title">
							<h2>Produk Terbaru</h2>
							<p>Jangan ketinggalan produk recycle terbaru ini.</p>
						</div>
					</div>
					<div class="col-lg-12">
						<form action="{{ route('front.index') }}" method="get">
							<div class="input-group">
								<input type="text" name="q" class="form-control" placeholder="Cari..." value="{{ request()->q }}">
								<div class="input-group-append">
									<button class="btn btn-secondary" type="button">Cari</button>
								</div>
							</div>
						</form>
						<br>
						<br>
					</div>
				</div>
				<div class="row">
                    @forelse($products as $row)
					<div class="col col1">
						<div class="f_p_item">
							<div class="f_p_img">
                                <img class="img-fluid" src="{{ asset('storage/products/' . $row->image) }}" alt="{{ $row->name }}">
								<div class="p_icon">
									<a href="{{ url('/product/' . $row->slug) }}">
										<i class="lnr lnr-cart"></i>
									</a>
								</div>
							</div>
                            <a href="{{ url('/product/' . $row->slug) }}">
                                <h4>{{ $row->name }}</h4>
							</a>
                            <h5>Rp {{ number_format($row->price) }}</h5>
						</div>
					</div>
                    @empty

                    @endforelse
				</div>

				<div class="row">
					
				</div>
			</div>
		</div>
	</section>
	<!--================End Feature Product Area =================-->

	<!--================Artikel Area =================-->
	
		<div class="main_box">
			<div class="container-fluid">
				<div class="row">
					<div class="main_title">
						<h2>Artikel terbaru</h2>
						<p>Tingkatkan wawasan anda lewat beberapa artikel di bawah ini.</p>
					</div>
				</div>
				<div class="autoplay">
					@forelse ($artikel as $row)
					<div>
						<div class="card mb-4">
						<img class="card-img-top" src="{{  $row->thumbnail }}" alt="{{ $row->title }}">
						<div class="card-body">
							<h2 class="card-title">{{ $row->title }}</h2>
							<p class="card-text">{!! \Illuminate\Support\Str::limit($row->content, $limit = 250, $end = '...</p>') !!}</p>
							<a href="{{  url('/artikel/' . $row->id)  }}" class="btn btn-success">Baca lebih lengkap &rarr;</a>
						</div>
						<div class="card-footer text-muted">
						{{ $row->created_at }}
							<a href="#">{{ $row->name }}</a>
						</div>
						</div>
					</div>
						@empty
							<div class="col">
								<h3 class="text-center">Tidak ada artikel</h3>
							</div>
						@endforelse
				</div>
				
				
			</div>
		</div>

	<!--================Remarket Area =================-->
	<section class="section_gap">
		<div class="section-title justify-content-left">
				<div class="main_title text-left">
					<h2>Apa itu Re:market?</h2>
					<p>Re:market adalah situs jual beli online pertama di Indonesia yang menjual beragam
					produk dengan mengusung konsep recycling. Visi Re:market adalah menjual produk upcycling maupun 
					downcycling sehingga mengurangi sampah di bumi dan menjadikan bumi lebih sehat lagi. Re:market mengajak 
					para UKM dibidang recycling untuk memasarkan produk-produknya di Re:Market.
					</p>
				</div>
			</div>
			<div class="section-title justify-content-left">
					<div class="main_title text-left">
						<h2>Jenis sampah apa saja yang dapat didaur ulang?</h2>
						<p>Ada berbagai jenis sampah, jika dilihat dari bentuknya maka akan diklasifikasikan menjadi: </p>
						<img src="{{ asset('ecommerce/img/sampahh.jpg') }}" alt="">
						<p><br/></p>
				</div>
			</div>
		<div class="main_title text-left">
			<h2>Mengapa harus menggunakan Re:market?</h2>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-2">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/logo/gembok.png') }}" alt="">
						<div class="content text-center">
							<h4><br/>Jaminan Keamanan</h4>
							<p>Diiisi oleh penjual-penjual terpercaya sehingga keamanan transaksi dapat terjamin.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="hot_deal_box">
						<img class="img-fluid" src= "{{ asset('ecommerce/img/logo/pengiriman.png') }}" alt="">
						<div class="content text-center">
							<h4><br/>Beragam Jasa Pengiriman</h4>
							<p>Memiliki beragam pilihan jasa pengiriman sehingga
							dapat menyesuaikan kebutuhan pengguna.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="hot_deal_box">
						<img class="img-fluid" src="{{ asset('ecommerce/img/logo/dompet.png') }}" alt="">
						<div class="content text-center">
							<h4><br/>Kemudahan Pembayaran</h4>
							<p>Re:market menyediakan berbagai metode pembayaran untuk bertransaksi.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="hot_deal_box">
						<img class="img-fluid" src= "{{ asset('ecommerce/img/logo/love.png') }}" alt="">
						<div class="content text-center">
							<h4><br/>Membuat Bumi Menjadi Lebih Baik</h4>
							<p>Barang hasil daur ulang dapat mengurangi limbah sehingga membuat
							bumi menjadi lebih baik.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
		</section>
	<!--================Remarket Area =================-->
	
	<!--================End area Area =================-->

@endsection

@section('js')
    <script>
		$('.autoplay').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: true,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 2000,
		});
		$('.autoplay').on('swipe', function(event, slick, direction){
  		console.log(direction);
		});
	</script>
@endsection