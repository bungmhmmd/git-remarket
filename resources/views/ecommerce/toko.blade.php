@extends('layouts.ecommerce')

@section('title')
    <title>{{ $profile_toko->nama_toko }} - Re:market</title>
@endsection

@section('content')
	<!--================Home Banner Area =================-->
	<section class="banner_area" >
		<div class="banner_inner d-flex align-items-center"
			 style="
					 background: linear-gradient(rgba(0,0,0,.5), rgba(0,0,0,.5)),
					 @if($profile_toko->banner !== null)
					 @php echo "url('".asset('/tokosaya/'.$profile_toko->username_toko.'/'.$profile_toko->banner)."');" @endphp
					 @else
				 	@php echo "url('".asset('/tokosaya/2.png')."');" @endphp
					 @endif

		background-repeat: no-repeat;
		background-size: cover;
		background-position: center center;
		color: #fff;
		height: 450px;
					 ">

			<div class="container">
				<div class="banner_content text-center" >
					<h2>{{ $profile_toko->nama_toko }}</h2>
				</div>

			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Category Product Area =================-->
	<section class="cat_product_area section_gap">

		<div class="container-fluid">
			@if($statustoko->status_toko == 2 || $statustoko->status_toko == 0)
				<div class="alert alert-danger">TOKO TIDAK AKTIF!</div>
			@endif
			<div class="col flex-col-reverse">
				<div class="row-lg-2">
					<div class="left_sidebar_area text-center border">
						<aside class="center_widgets cat_widgets">
							<div class="card-profile-image" style="margin-bottom: 40px;margin-top: 30px">
								@if($profile_toko->avatar == null)
									<img src="{{asset('tokosaya/1.jpg')}}" alt="image" id="avatar" class="card-img-top img-fluid "
										 style="
                                        width: 80%;
                                        height: 80%;
                                        max-width:350px;
                                        max-height:350px;
                                        object-fit:scale-down;
                                        border-radius:50% 50% 50% 50%;
                                        ">
								@else
									<img src="{{asset('/tokosaya/'.$profile_toko->username_toko).'/'.$profile_toko->avatar}}" alt="image" id="avatar"  class="card-img-top img-fluid "
										 style="
                                       width: 80%;
                                        height: 80%;
                                        max-width:350px;
                                        max-height:350px;
                                        object-fit:scale-down;
                                        border-radius:50% 50% 50% 50%;
                                        ">
								@endif

							</div>
							<div class="l_w_title">
								<h2>{{ $profile_toko->nama_toko }}</h2>
								<p class="text-muted">Username:{{ $profile_toko->username_toko }}</p>
								<div class="strong">Deskripsi Toko:</div>
								<p class="text">{!! $profile_toko->deskripsi_toko !!}</p>
								<p>Alamat: {{ $profile_toko->address }}</p>
							</div>
							<div class="widgets_inner">
								<ul class="list" >
									<li>
										
									</li>
									<div class="form-group">
										<div class="row">
											<div class="col-6">	<label for="" style="margin: 8px">Propinsi:</label></div>
											<div class="col-6">
												<select class="bg-white text-dark form-control" name="province_id" id="province_id" disabled style="-webkit-appearance: none;">
													<option value=""></option>
													@if(!empty($customer->district))
													@foreach ($provinces as $row)
														<option value="{{ $row->id }}" {{ $customer->district->province_id == $row->id ? 'selected':'' }}>{{ $row->name }}</option>
													@endforeach
														@endif
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-6">	<label for="" style="margin: 8px">Kabupaten / Kota</label></div>
											<div class="col-6">
												<select class="bg-white text-dark form-control" name="city_id" id="city_id" disabled style="-webkit-appearance: none;">
													<option value=""></option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-6">	<label for="" style="margin: 8px">Kecamatan </label></div>
											<div class="col-6">
												<select class="bg-white text-dark form-control" name="district_id" id="district_id" disabled style="-webkit-appearance: none;">
													<option value=""></option>
												</select>
											</div>
										</div>
									</div>
								</ul>
							</div>
						</aside>
					</div>
				</div>
				<div class="row-lg-5">
					<div class="product_top_bar">
						<div class="left_dorp">
							<select class="sorting" onchange="location = this.value;" style="width: auto">
								<option value="{{ url('/toko/' . $profile_toko->username_toko.'/all') }}">Diurut dari produk terbaru</option>
								<option value="{{ url('/toko/' . $profile_toko->username_toko.'/terlama') }}">Diurut dari produk terlama</option>
							</select>
							<select class="show" onchange="location = this.value;" style="width: auto">
								<option value="{{ url('/toko/' . $profile_toko->username_toko.'/all') }}">Tampilkan 12</option>
                                <option value="{{ url('/toko/' . $profile_toko->username_toko.'/14') }}">Tampilkan 14</option>
                                <option value="{{ url('/toko/' . $profile_toko->username_toko.'/16') }}">Tampilkan 16</option>
							</select>

							<select id="kategori" class="show" onchange="location = this.value;" style="width: auto">
								<option value="{{ url('/toko/' . $profile_toko->username_toko.'/all') }}">Kategori : Semua</option>
								@foreach ($categories as $category)
									<option value="{{ url('/toko/' . $profile_toko->username_toko.'/'.$category->slug) }}">Kategori : {{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="right_page ml-auto">
							{{ $products->links() }}
						</div>
					</div>
					<div class="latest_product_inner row">
						@forelse ($products as $row)
							<div class="col-lg-3 col-md-3 col-sm-6">
								<div class="f_p_item">
									<div class="f_p_img">
										<img class="img-fluid" src="{{ asset('storage/products/' . $row->image) }}" alt="{{ $row->name }}">
										<div class="p_icon">
											<a href="{{ url('/product/' . $row->slug) }}">
												<i class="lnr lnr-cart"></i>
											</a>
										</div>
									</div>
									<a href="{{ url('/product/' . $row->slug) }}">
										<h4>{{ $row->name }}</h4>
									</a>
									<h5>Rp {{ number_format($row->price) }}</h5>
								</div>
							</div>
						@empty
							<div class="col-md-12">
								<h3 class="text-center">Tidak ada produk</h3>
							</div>
						@endforelse
					</div>
				</div>
			</div>

			<div class="row">
				{{ $products->links() }}
			</div>
		</div>
	</section>
	<!--================End Category Product Area =================-->


@endsection

@section('js')
	<script>
        $(document).ready(function(){
            loadCity($('#province_id').val(), 'bySelect').then(() => {
                loadDistrict($('#city_id').val(), 'bySelect');
            })
        })

        $('#province_id').on('change', function() {
            loadCity($(this).val(), '');
        })

        $('#city_id').on('change', function() {
            loadDistrict($(this).val(), '')
        })

        function loadCity(province_id, type) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: "{{ url('/api/city') }}",
                    type: "GET",
                    data: { province_id: province_id },
                    success: function(html){
                        $('#city_id').empty()
                        $('#city_id').append('<option value=""></option>')
                        $.each(html.data, function(key, item) {
                            @if($profile_toko->district_id !== 0)
                            let city_selected = {{ $customer->district->city_id }};
                            @else
                            let city_selected = null;
									@endif
                            let selected = type == 'bySelect' && city_selected == item.id ? 'selected':'';
                            $('#city_id').append('<option value="'+item.id+'" '+ selected +'>'+item.name+'</option>')
                            resolve()
                        })
                    }
                });
            })
        }

        function loadDistrict(city_id, type) {
            $.ajax({
                url: "{{ url('/api/district') }}",
                type: "GET",
                data: { city_id: city_id },
                success: function(html){
                    $('#district_id').empty()
                    $('#district_id').append('<option value=""></option>')
                    $.each(html.data, function(key, item) {

								@if($profile_toko->district_id !== 0)
                        let district_selected = {{ $customer->district->id }};
								@else
                        let district_selected = null;
								@endif
                        let selected = type == 'bySelect' && district_selected == item.id ? 'selected':'';
                        $('#district_id').append('<option value="'+item.id+'" '+ selected +'>'+item.name+'</option>')
                    })
                }
            });
        }

        $(document).ready(function(){
            $('#kategori').val(location.href);   //  assign URL param to select field
        });
	</script>
@endsection