@extends('layouts.ecommerce')

@section('title')
    <title>Keranjang Belanja - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
		<div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Keranjang Belanja</h2>
					<div class="page_link">
                        <a href="{{ url('/') }}">Beranda</a>
                        <a href="{{ route('front.list_cart') }}">Keranjang Belanja</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Cart Area =================-->
	<section class="cart_area">
		<div class="container">
			<div class="cart_inner">
				<a class="gray_btn" href="{{ route('front.product') }}">Continue Shopping</a>
				<div class="table-responsive">
					<form action="{{ route('front.update_cart') }}" method="post">
						@csrf
						@foreach($toko_username as $toko)
					<table class="table border">

						<thead>
						<tr style="text-align: center" class="border border-bottom">
							<th colspan="4">
								<h3> Toko : {{$toko['nama_toko']}}</h3>
							</th>
						</tr>
							<tr>
								<th scope="col">Product</th>
								<th scope="col">Price</th>
								<th scope="col">Quantity</th>
								<th scope="col">Total</th>
							</tr>
						</thead>
						<tbody>
						@forelse ($carts as $row)

								@if($row['username_toko'] == $toko['username_toko'])
							<tr>
								<td>
									<div class="media">
										<div class="d-flex">
                                            <img src="{{ asset('storage/products/' . $row['product_image']) }}" width="100px" height="100px" alt="{{ $row['product_name'] }}">
										</div>
										<div class="media-body">
                                            <p>{{ $row['product_name'] }}</p>
										</div>
									</div>
								</td>
								<td>
                                    <h5>Rp {{ number_format($row['product_price']) }}</h5>
								</td>
								<td>
									<div class="product_count">
                                        <input type="text" name="qty[]" id="sst{{ $row['product_id'] }}" maxlength="12" value="{{ $row['qty'] }}" title="Quantity:" class="input-text qty">
                                        <input type="hidden" name="product_id[]" value="{{ $row['product_id'] }}" class="form-control">
										<button onclick="var result = document.getElementById('sst{{ $row['product_id'] }}'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
										 class="increase items-count" type="button">
											<i class="lnr lnr-chevron-up"></i>
										</button>
										<button onclick="var result = document.getElementById('sst{{ $row['product_id'] }}'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
										 class="reduced items-count" type="button">
											<i class="lnr lnr-chevron-down"></i>
										</button>
									</div>
								</td>
								<td>
                                    <h5>Rp {{ number_format($row['product_price'] * $row['qty']) }}</h5>
								</td>
                            </tr>
							@endif
                            @empty
                            <tr>
                                <td colspan="4">Tidak ada belanjaan</td>
                            </tr>
                            @endforelse
							<tr class="bottom_button">
								<td>
								<button class="btn btn-primary">Update Cart</button>
									@if (auth()->guard('customer')->check())
										<a class="btn btn-success" href="{{ url('/checkout/'. $toko['username_toko']) }}">Bayar toko ini</a>
									@endif
								</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>

							<tr class="out_button_area">
								<td></td>
								<td></td>
								<td></td>
								<td>
								</td>
							</tr>
						</tbody>

					</table>
					@endforeach
					@if (auth()->guard('customer')->check())
					@else
						<a class="btn btn-success" href="{{ route('customer.login') }}">Login Untuk Melanjukan Transaksi</a>
					@endif
					</form>
				</div>
			</div>
		</div>
	</section>
	<!--================End Cart Area =================-->
@endsection