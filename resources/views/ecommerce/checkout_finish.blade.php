@extends('layouts.ecommerce')

@section('title')
    <title>Keranjang Belanja - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
    <section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
		<div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Pesanan Diterima</h2>
					<div class="page_link">
                        <a href="{{ url('/') }}">Home</a>
						<a href="">Invoice</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Order Details Area =================-->
	<section class="order_details p_120">
		<div class="container">
			<h3 class="title_confirmation">Terima kasih, pesanan anda telah kami terima.</h3>
			<div class="row order_d_inner">
				<div class='row'>
					<div class="col-lg-6">
						<div class="details_item">
							<h4>Informasi Pesanan</h4>
							<ul class="list">
								<li>
									<a href="#">
										<span>Invoice</span> : {{ $order->invoice }}</a>
								</li>
								<li>
									<a href="#">
										<span>Tanggal</span> : {{ $order->created_at }}</a>
								</li>
								<li>
									<a href="#">
										<span>Subtotal</span> : Rp {{ number_format($order->subtotal) }}
									</a>
								</li>
								<li>
									<a href="#">
										<span>Ongkos Kirim</span> : Rp {{ number_format($order->cost) }}
									</a>
								</li>
								<li>
									<a href="#">
										<span>Total</span> : Rp {{ number_format($order->total) }}
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="details_item">
							<h4>Informasi Pemesan</h4>
							<ul class="list">
								<li>
									<a href="#">
										<span>Alamat</span> : {{ $order->customer_address }}</a>
								</li>
								<li>
									<a href="#">
										<span>Kota</span> : {{ $order->district->city->name }}</a>
								</li>
								<li>
									<a href="#">
										<span>Country</span> : Indonesia</a>
								</li>
							</ul>
						</div>
					</div>
						<div class="col-md-12 mt-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Cara Pembayaran</h4>
                                </div>
                                <div class="card-body">
									<strong>Jumlah yang harus dibayar adalah: Rp {{ number_format($order->total) }}</strong><br>
									<strong>Transfer ke salah satu rekening dibawah ini. Simpan struk transfer untuk konfirmasi pembayaran</strong>				
									<div class="table-responsive">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th>Nomor Rekening</th>
													<th>Atas Nama</th>
													<th>Bank</th>
												</tr>
											</thead>
											<tbody>
												@forelse ($norek as $row)
												<tr>
													<td><strong>{{ $row->no_rek }}</strong></td>
													<td>
														<strong>{{ $row->atasnama }}</strong><br>
													</td>
													<td>
														<strong>{{ $row->bank }}</strong><br>
													</td>
												</tr>
													@empty
												<tr>
													<td colspan="6" class="text-center">Tidak ada data</td>
												</tr>
												@endforelse
											</tbody>
										</table>
										<strong>Jika sudah transfer, lakukan konfirmasi pembayaran dengan cara:</strong>
										<ol>
											<li>
												Buka halaman dashboard.
											</li>
											<li>
												Pilih menu pesanan. 
											</li>
											<li>
												Klik tombol detail pada pesanan yang berinvoice {{ $order->invoice }}.
											</li>
											<li>
												Klik tombol konfirmasi pembayaran.
											</li>
											<li>
												Isi kolom nama pengirim, jumlah tranfer, dan tanggal transfer.
											</li>
											<li>
												Pada kolom transfer ke pilih nomor rekening tujuan yang ditransfer.
											</li>
											<li>
												Pada kolom bukti transfer upload struk transfer.
											</li>
											<li>
												Terakhir, klik tombol konfirmasi.
											</li>
										</ol>
									</div>
								</div>
                            </div>
                        </div>
				</div>
			</div>
		</div>
	</section>
    <!--================End Order Details Area =================-->
    
@endsection