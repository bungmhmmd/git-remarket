@extends('layouts.ecommerce')

@section('title')
    <title>Wishlist - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
		<div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Wishlist</h2>
					<div class="page_link">
                        <a href="{{ url('/') }}">Home</a>
                        <a href="{{ route('front.list_wishlist') }}">Wishlist</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Cart Area =================-->
	<section class="cart_area">
		<div class="container">
			<div class="cart_inner">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Product</th>
								<th scope="col">Price</th>
							</tr>
						</thead>
						<tbody>
                            @forelse ($wishlist as $row)
							<tr>
								<td>
									<div class="media">
										<div class="d-flex">
                                            <img src="{{ asset('storage/products/' . $row->product_img) }}" width="100px" height="100px" alt="{{ $row->product_name }}">
										</div>
										<div class="media-body">
											<a href="{{ url('/product/' . $row->product_name) }}">
												<h4>{{ $row->product_name }}</h4>
											</a>
											<p>{{ $row->product_toko }}</p>
										</div>
									</div>
								</td>
								<td>
                                    <h5>Rp {{ number_format($row->product_price) }}</h5>
								</td>
								<td>
									<form action="{{ route('front.delete_wishlist') }}" method="POST">
									@csrf
										<input type="hidden" name="wishlist_id" value="{{ $row->id }}" class="form-control">
										<button class="main_btn">Hapus</button>
									</form>
								
								
								</td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4">Produk wishlist anda kosong.</td>
                            </tr>
                            @endforelse		
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
@endsection