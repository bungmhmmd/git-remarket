@extends('layouts.ecommerce')

@section('title')
    <title>Artikel - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Artikel</h2>
                    <div class="page_link">
                        <a href="{{ route('front.index') }}">Home</a>
                        <a href="{{ route('artikel.list_artikel') }}">Artikel</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Artikel Area =================-->
    <section class="cat_product_area section_gap">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-7" >
                @forelse ($artikel as $row)
                    <div class="card mb-4">
                    <img class="card-img-top" src="{{  $row->thumbnail }}" alt="{{ $row->title }}">
                    <div class="card-body">
                        <h2 class="card-title">{{ $row->title }}</h2>
                        <p class="card-text">{!! \Illuminate\Support\Str::limit($row->content, $limit = 250, $end = '...</p>') !!}</p>
                        <a href="{{  url('/artikel/' . $row->id)  }}" class="btn btn-success">Baca lebih lengkap &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                    {{ $row->created_at }}
                        <a href="#">{{ $row->name }}</a>
                    </div>
                    </div>
                    @empty
                        <div class="col">
                            <h3 class="text-center">Tidak ada artikel</h3>
                        </div>
                     @endforelse
                     {!! $artikel->links() !!} 
                    </div>
                <div class="col"></div>
                
            </div> 
            
        </div>
    </section>
    <!--================Artikel Area =================-->
@endsection