@extends('layouts.ecommerce')

@section('title')
    <title>Artikel - Re:market</title>
@endsection


@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Artikel</h2>
                    <div class="page_link">
                        <a href="{{ route('front.index') }}">Home</a>
                        <a href="{{ route('artikel.list_artikel') }}">Artikel</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

<!-- Games section -->
<section class="cat_product_area section_gap">
        <div class="container">
            <div class="row">
                
                    <div class="col-lg" >
                        <div class="card mb-4">
                            <div class="card-header text-muted">
                                <h2 class="card-title">{{ $artikel->title }}</h2>
                                {{ $artikel->created_at }}
                                <a href="#">{{ $artikel->name }}</a>
                            </div>
                            <img class="card-img-top" src="{{  $artikel->thumbnail }}" alt="{{ $artikel->title }}">                            
                        </div>
                        <div class="card mb-4">
                            <div class="card-body">
                                <p class="card-text">{!! $artikel->content !!}</p>
                            </div>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
    </section>
<!-- Games end-->
@endsection