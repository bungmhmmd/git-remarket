@extends('layouts.ecommerce')

@section('title')
    <title>Kontak - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
      <div class="overlay"></div>
				<div class="banner_content text-center">
					<h2>Kontak</h2>
					<div class="page_link">
                        <a href="{{ url('/') }}">Beranda</a>
                        <a href="{{ route('customer.login') }}">Kontak</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Login Box Area =================-->
    <section id="contact" class="contact section_gap">
      <div class="container">
      <div class="container-fluid">
      
      <div class="row">
            @if (session('success'))
			<div class="alert alert-success mt-2">{{ session('success') }}</div>
			@endif
      </div>

      <div class="row">
            <aside class="left_widgets cat_widgets">
                <div class="l_w_title">
                    <h3><strong>Kontak</strong></h3>
                </div>
                    
                     <div class="widgets_inner">
                            <div class="alamat">
                                <i class="icofont-google-map"></i>
                                <h5>Alamat</h5>
                                <p>Jl. Kedung Raya No. 41</p>
                            </div>

                            <div class="email">
                                <i class="icofont-envelope"></i>
                                <h5>Email:</h5>
                                <p>contact@remarket.com</p>
                            </div>

                            <div class="phone">
                                <i class="icofont-phone"></i>
                                <h5>No. Telpon</h5>
                                <p>+6281111222333</p>
                            </div>

                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.212424279516!2d106.83513221401682!3d-6.366549395393026!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ec1235c5a7d3%3A0xbd2b943aae817e23!2sJl.%20Kedoya%20Raya%2C%20Pondok%20Cina%2C%20Kecamatan%20Beji%2C%20Kota%20Depok%2C%20Jawa%20Barat%2016424!5e0!3m2!1sen!2sid!4v1597400860461!5m2!1sen!2sid" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
                    </div>
            </aside>
          
        @if (auth()->guard('customer')->check())
          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
          <form class="php-email-form" action="{{ route('front.addpesan') }}" method="post" novalidate="novalidate" role="form" >
              @csrf
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Nama Pengguna</label>
                  <input type="text" name="nama" class="form-control" id="name" value="{{ auth()->guard('customer')->user()->name }}"  data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Email</label>
                  <input type="email" class="form-control" name="email" id="email" value="{{ auth()->guard('customer')->user()->email }}" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subjek</label>
                <input type="text" class="form-control" name="subjek" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="name">Pesan</label>
                <textarea class="form-control" name="pesan" rows="10" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="validate"></div>
              </div>
            
              <div class="checkout_btn_inner">
                <button type="submit" value="submit" class="btn submit_btn">Kirim</button>
              </div>
            </form>
            

          </div>

          @else
          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form class="php-email-form" action="{{ route('front.addpesan') }}" method="post" novalidate="novalidate" role="form" >
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">Nama Pengguna</label>
                  <input type="text" name="nama" class="form-control" id="name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Email</label>
                  <input type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subjek</label>
                <input type="text" class="form-control" name="subjek" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <label for="name">Pesan</label>
                <textarea class="form-control" name="pesan" rows="10" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="validate"></div>
              </div>
            
              <div class="checkout_btn_inner">
                <a class="main_btn" href="{{ route('front.checkout') }}">Login untuk mengirim pesan</a>
			</div>
            </form>
          </div>

          @endif

        </div>
        </div>
      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

@endsection

