@extends('layouts.ecommerce')

@section('title')
    <title>Login - Re:market</title>
@endsection

@section('content')
    <!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
		<div class="overlay"></div>
			<div class="container">
				<div class="banner_content text-center">
					<h2>Login</h2>
					<div class="page_link">
                        <a href="{{ url('/') }}">Home</a>
                        <a href="{{ route('customer.login') }}">Login</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Login Box Area =================-->
	<section class="login_box_area p_120">
		<div class="container">
			<div class="row">
				<div class="offset-md-3 col-lg-6">
					<?php
						if(session('regis_m') === true){
						echo '<div class="alert alert-success shadow">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Anda sudah terdaftar, silahkan login.</strong>
						</div>';
						}
					?>
                    @if (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

					<div class="login_form_inner">
						<h3>Login untuk masuk</h3>
						<form class="row login_form" action="{{ route('customer.post_login') }}" method="post" id="contactForm" novalidate="novalidate">
							@csrf
							<div class="col-md-12 form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
							</div>
							<div class="col-md-12 form-group">
								<input type="password" class="form-control" id="password" name="password" placeholder="******">
							</div>
							<div class="col-md-12 form-group">
								
								<button type="submit" value="submit" class="btn submit_btn">Log In</button>
								<a class="btn submit_btn text-white" href="{{ route('customer.register') }}">Registrasi Akun Baru</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection