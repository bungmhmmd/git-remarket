<nav class="sidebar-nav">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('category.index') }}">
                <i class="nav-icon icon-drop"></i> Kategori
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('aktivasitoko.index') }}">
                <i class="nav-icon icon-drop"></i> Aktivasi Toko
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('artikel.index') }}">
                <i class="nav-icon icon-drop"></i> Artikel
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('pesan') }}">
                <i class="nav-icon icon-drop"></i> Pesan
            </a>
        </li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="javascript">
                <i class="nav-icon icon-settings"></i> Laporan
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('report.order') }}">
                        <i class="nav-icon icon-puzzle"></i> Pemesanan
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('report.return') }}">
                        <i class="nav-icon icon-puzzle"></i> Pengembalian
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>