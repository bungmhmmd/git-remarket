<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<link href="{{ asset('ecommerce/img/logo-remarket.png') }}" rel="icon">
    
    @yield('title')

	<link rel="stylesheet" href="{{ asset('ecommerce/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/linericon/style.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/lightbox/simpleLightbox.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/nice-select/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/jquery-ui/jquery-ui.css') }}">

	<link rel="stylesheet" href="{{ asset('ecommerce/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/css/responsive.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('ecommerce/vendors/slick/slick-theme.css') }}">
	<link href="css/blog-home.css" rel="stylesheet">

	<style>
		.menu-sidebar-area {
			list-style-type:none; padding-left: 0; font-size: 15pt;
		}
		.menu-sidebar-area > li {
			margin:0 0 10px 0;
			list-style-position:inside;
			border-bottom: 1px solid black;
		}
		.menu-sidebar-area > li > a {
			color: black
		}
		/* Style the sidenav links and the dropdown button */
		.sidenav a, .dropdown-btn {
			text-decoration: none;
			font-size: 20px;
			display: block;
			border: none;
			background: none;
			width: 100%;
			text-align: left;
			cursor: pointer;
			outline: none;
		}

		/* On mouse-over */
		.sidenav a:hover, .dropdown-btn:hover {
			color: #f1f1f1;
		}

		/* Main content */
		.main {
			margin-left: 200px; /* Same as the width of the sidenav */
			font-size: 20px; /* Increased text to enable scrolling */
			padding: 0px 10px;
		}

		/* Add an active class to the active dropdown button */
		/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
		.dropdown-container {
			display: none;
			padding-left: 12px;
			margin-top: 5px;

		}

		.nav-tabs > li.active > a {
			color: #ffffff;
			font-size: 30px;
		}
		.nav-tabs > li > a {
			color: #7f7f7f;
		}
		.nav-tabs > .active > a,
		.nav-tabs > .active > a:hover,
		.nav-tabs > .active > a:focus {
			background-color: #3c5a78;
			border-top: 1px solid #777;
			border-right: 1px solid #777;
			border-left: 1px solid #777;
			border-radius: 15px 15px 0px 0px;
			padding-top: 5px;
			padding-right: 10px;
			padding-left: 10px;
		}
		/* Optional: Style the caret down icon */
		.fa-caret-down {
			float: right;
			padding-right: 8px;
		}

		/* Some media queries for responsiveness */
		@media screen and (max-height: 450px) {
			.sidenav {padding-top: 15px;}
			.sidenav a {font-size: 18px;}
		}
	</style>
	@yield('css')
</head>

<body>
	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="top_menu row m0">
			<div class="container-fluid">
				<div class="float-right">
					<ul class="right_side">
						@if (auth()->guard('customer')->check())
						    <li><a href="{{ route('customer.dashboard') }}">{{ auth()->guard('customer')->user()->name }}</a></li>
							<li><a href="{{ route('customer.logout') }}">Logout</a></li>
						@else
							<li><a href="{{ route('customer.login') }}">Login</a></li>
						@endif
					</ul>
				</div>
			</div>
		</div>
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="{{ url('/') }}">
						<img src="{{ asset('ecommerce/img/logo.png') }}" alt="">
					</a>

					<div class="navbar-light col-lg-4">
					<form class="form-inline" action="{{ route('front.product') }}" method="get">
					@csrf
						<div class="input-group">
							<input type="hidden" name="sorting" value="{{ session('sorting') }}" class="form-control">
							<input type="hidden" name="show" value="{{ session('show') }}" class="form-control">
							<input class="form-control mr-sm-2" type="text" name="q" class="form-control" placeholder="Cari produk atau toko" value="{{ request()->q }}">
							<div class="input-group-append">
								<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
							</div>
						</div>
					</form>
					</div>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					 aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<div class="row w-100">
							<div class="col-lg-7 pr-0">
								@include('layouts.ecommerce.module.menu')
							</div>

							<div class="col-lg-5">
								<ul class="nav navbar-nav navbar-right right_nav pull-right">
									<hr>
									<li class="nav-item">
										<a href="{{ route('front.list_cart') }}" class="icons">
											<i class="lnr lnr lnr-cart"></i>
										</a>
									</li>
									<hr>
									<li class="nav-item">
										<a href="{{ route('front.list_wishlist') }}" class="icons">
											<i class="fa fa-heart-o" aria-hidden="true"></i>
										</a>
									</li>
									<hr>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

    @yield('content')
    
    <!--================ start footer Area  =================-->
	<footer class="footer-area section_gap">
		<div class="container-fluid">
				<a class="navbar-brand logo_h" href="{{ route('front.index') }}">
					<img height="100" width="200" src="{{ asset('ecommerce/img/logo.png') }}" alt="">
				</a>
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6 class="footer_title ">Kategori</h6>
						<ul class="list" >
                                    @foreach ($categories as $category)
                                    <li>
                                        <strong><a href="{{ url('/category/' . $category->slug) }}">{{ $category->name }}</a></strong>
                                        
                                        @foreach ($category->child as $child)
                                        <ul class="list" style="display: block">
                                            <li>
                                                <a href="{{ url('/category/' . $child->slug) }}">{{ $child->name }}</a>
                                            </li>
                                        </ul>
                                        @endforeach
                                    </li>
                                    @endforeach
                                </ul>     
					</div>
				</div>
				
				<div class="col-lg-5 col-md-6 col-sm-6">
					<div class="single-footer-widget instafeed">
						<h6 class="footer_title">Kontak</h6>
						<div class="row"> </div>
                            <div class="alamat">
                                <i class="icofont-google-map"></i>
                                <h5>Alamat</h5>
                                <p>Jl. Kedung Raya No. 41</p>
                            </div>

                            <div class="email">
                                <i class="icofont-envelope"></i>
                                <h5></br>Email:</h5>
                                <p>contact@remarket.com</p>
                            </div>

                            <div class="phone">
                                <i class="icofont-phone"></i>
                                <h5></br>No. Telpon</h5>
                                <p>+6281111222333</p>
                            </div>
						</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="single-footer-widget f_social_wd">
						<h6 class="footer_title">Ikuti Kami</h6>
						<div class="f_social">
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
				</div>
					</div>
				</div>
			</div>
			<div class="row footer-bottom d-flex justify-content-between align-items-center">
				<p class="col-lg-12 footer-text text-center">
					&copy;<script>document.write(new Date().getFullYear());</script> Copyright <a>Re:market</a>
				</p>
			</div>
		</div>
	</footer>
	<!--================ End footer Area  =================-->

	<script src="{{ asset('ecommerce/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('ecommerce/js/popper.js') }}"></script>
	<!-- boostrap js lama <script src="{{ asset('ecommerce/js/bootstrap.min.js') }}"></script> -->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('ecommerce/js/stellar.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/lightbox/simpleLightbox.min.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/isotope/imagesloaded.pkgd.min.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/isotope/isotope-min.js') }}"></script>
	<script src="{{ asset('ecommerce/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/counter-up/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/flipclock/timer.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/counter-up/jquery.counterup.js') }}"></script>
	<script src="{{ asset('ecommerce/js/mail-script.js') }}"></script>
	<script src="{{ asset('ecommerce/js/theme.js') }}"></script>
	<script src="{{ asset('ecommerce/vendors/slick/slick.min.js') }}"></script>
	<script>
        /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
        }
	</script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<script>
        $(document).ready(function() {
            let start = moment().startOf('month')
            let end = moment().endOf('month')

            $('#exportpdf_order').attr('href', '{{ url("member/reportstoko/order/pdf/") }}/' + start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'));

            $('#created_at_order').daterangepicker({
                startDate: start,
                endDate: end
            }, function(first, last) {
                $('#exportpdf_order').attr('href', '{{ url("member/reportstoko/order/pdf/") }}/' + first.format('YYYY-MM-DD') + '+' + last.format('YYYY-MM-DD'))
            });

            $('#exportpdf_return').attr('href', '{{ url("member/reportstoko/return/pdf/") }}/' + start.format('YYYY-MM-DD') + '+' + end.format('YYYY-MM-DD'));

            $('#created_at_return').daterangepicker({
                startDate: start,
                endDate: end
            }, function(first, last) {
                $('#exportpdf_return').attr('href', '{{ url("member/reportstoko/return/pdf/") }}/' + first.format('YYYY-MM-DD') + '+' + last.format('YYYY-MM-DD'))
            })

        })
	</script>
	@yield('js')
</body>
</html>