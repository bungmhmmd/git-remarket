<ul class="nav navbar-nav center_nav pull-right">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('front.index') }}">Beranda</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('front.product') }}">Produk</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('artikel.list_artikel') }}">Artikel</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('front.kontak') }}">Kontak</a>
    </li>
</ul>