<div class="card" style="background-color: #bdc3c7">
    <div class="card-body">
        <h3>Menu</h3>
        <ul class="menu-sidebar-area">
            <li class="icon-dashboard"><a href="{{ route('customer.dashboard') }}">Dashboard</a></li>
            <li class="icon-customers"><a href="{{ route('customer.orders') }}">Pesanan</a></li>
            <li class="icon-users"><a href="{{ route('customer.settingForm') }}">Pengaturan</a></li>
        @if(auth()->guard('customer')->user()->status_toko  != 1)
            <li class="icon-users"><a href="{{ route('customer.aktivasitoko') }}">Aktivasi Toko</a></li>
        @else
            <div class="sidenav">
                <a class="dropdown-btn text-dark">Toko
                    <i class="fa fa-caret-down"></i>
                </a>
                <div class="dropdown-container">
                    <a href="{{ route('customer.tokosaya') }}" class="text-dark">Toko Saya</a>
                    <a href="{{ route('customer.profiletoko') }}" class="text-dark">Profile Toko</a>
                </div>
            </div>
        @endif

        </ul>
    </div>
</div>