<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Ecommerce\FrontController@index')->name('front.index');
Route::get('/product', 'Ecommerce\FrontController@product')->name('front.product');
Route::get('/product/sort', 'Ecommerce\FrontController@sort_product')->name('front.sort_product');
Route::get('/product/{slug}', 'Ecommerce\FrontController@show')->name('front.show_product');
Route::get('/category/{slug}', 'Ecommerce\FrontController@categoryProduct')->name('front.category');
Route::get('/product/{slug}', 'Ecommerce\FrontController@show')->name('front.show_product');
//profile toko
Route::get('/toko/{username_toko}/{slug}', 'Ecommerce\FrontController@showtoko');
Route::post('cart', 'Ecommerce\CartController@addToCart')->name('front.cart');
Route::get('/cart', 'Ecommerce\CartController@listCart')->name('front.list_cart');
Route::post('/cart/update', 'Ecommerce\CartController@updateCart')->name('front.update_cart');

Route::get('/checkout/{toko}', 'Ecommerce\CartController@checkout')->name('front.checkout');
Route::post('/checkout', 'Ecommerce\CartController@processCheckout')->name('front.store_checkout');
Route::get('/checkout/{invoice}', 'Ecommerce\CartController@checkoutFinish')->name('front.finish_checkout');

Route::get('/kontak', 'Ecommerce\FrontController@kontak')->name('front.kontak');

Route::get('/artikel', 'ArtikelController@list_artikel')->name('artikel.list_artikel');
//Route::get('/artikel', 'ArtikelController@index')->name('artikel.index');
Route::get('/artikel/{blog_id}', 'ArtikelController@getFullPost')->name('post.read');

Route::get('/product/ref/{user}/{product}', 'Ecommerce\FrontController@referalProduct')->name('front.afiliasi');

Route::group(['prefix' => 'member', 'namespace' => 'Ecommerce'], function() {
    Route::get('login', 'LoginController@loginForm')->name('customer.login');
    Route::post('login', 'LoginController@login')->name('customer.post_login');
    Route::get('verify/{token}', 'FrontController@verifyCustomerRegistration')->name('customer.verify');

    Route::get('Register', 'RegisterController@registerForm')->name('customer.register');
    Route::post('Register', 'RegisterController@register')->name('customer.post_register');

    Route::group(['middleware' => 'customer'], function() {
        Route::get('dashboard', 'LoginController@dashboard')->name('customer.dashboard');
        Route::get('logout', 'LoginController@logout')->name('customer.logout');

        Route::post('/kontak/pesan', 'FrontController@addpesan')->name('front.addpesan');
        
        Route::get('/checkout', 'CartController@checkout')->name('front.checkout');
        Route::post('/checkout', 'CartController@processCheckout')->name('front.store_checkout');
        Route::get('/checkout/{invoice}', 'CartController@checkoutFinish')->name('front.finish_checkout');

        Route::get('orders', 'OrderController@index')->name('customer.orders');
        Route::get('orders/{invoice}', 'OrderController@view')->name('customer.view_order');
        Route::get('orders/pdf/{invoice}', 'OrderController@pdf')->name('customer.order_pdf');
        Route::post('orders/accept', 'OrderController@acceptOrder')->name('customer.order_accept');
        Route::get('orders/return/{invoice}', 'OrderController@returnForm')->name('customer.order_return');
        Route::put('orders/return/{invoice}', 'OrderController@processReturn')->name('customer.return');

        Route::get('payment/{invoice}', 'OrderController@paymentForm')->name('customer.paymentForm');
        Route::post('payment', 'OrderController@storePayment')->name('customer.savePayment');

        Route::get('setting', 'FrontController@customerSettingForm')->name('customer.settingForm');
        Route::post('setting', 'FrontController@customerUpdateProfile')->name('customer.setting');

        Route::get('/afiliasi', 'FrontController@listCommission')->name('customer.affiliate');

        Route::post('wishlist', 'FrontController@addTowishlist')->name('front.wishlist');
        Route::get('/wishlist', 'FrontController@wishlist')->name('front.list_wishlist');
        Route::post('/hapuswishlist', 'FrontController@deletewishlist')->name('front.delete_wishlist');

        //aktivasi toko
        Route::get('/aktivasitoko', 'FrontController@aktivasitoko')->name('customer.aktivasitoko');
        Route::post('/kirimaktivasi', 'FrontController@kirimaktivasi')->name('customer.kirimaktivasi');
        //tokosaya
        Route::resource('product', 'ProductController')->except(['show']);
        Route::get('/tokosaya', 'FrontController@tokosaya')->name('customer.tokosaya');
        Route::get('/profiletoko', 'FrontController@profiletoko')->name('customer.profiletoko');
        Route::post('profiletoko', 'FrontController@profiletokoUpdateProfile')->name('customer.profiletoko.update');
        Route::post('profiletoko/hapusava', 'FrontController@hapusava')->name('customer.profiletoko.hapusava');
        Route::post('profiletoko/uploudava', 'FrontController@uploadava')->name('customer.profiletoko.uploudava');
        Route::post('profiletoko/hapusbanner', 'FrontController@hapusbanner')->name('customer.profiletoko.hapusbanner');
        Route::post('profiletoko/uploudbanner', 'FrontController@uploadbanner')->name('customer.profiletoko.uploudbanner');
        Route::post('/aktifkantoko', 'FrontController@aktifkantoko')->name('customer.aktifkantoko');

        //bank
        Route::get('/tambahbank', 'FrontController@createbank')->name('customer.createbank');
        Route::post('/postbank', 'FrontController@storebank')->name('customer.storebank');
        Route::post('/hapusbank', 'FrontController@hapusbank')->name('customer.hapusbank');
        Route::get('/editbank/{id}', 'FrontController@editbank')->name('customer.editbank');
        Route::put('/updatebank/{id}', 'FrontController@updatebank')->name('customer.updatebank');



        //product cusomter
        Route::get('/tambahproduk', 'FrontController@createproduk')->name('customer.createproduk');
        Route::post('/postproduk', 'FrontController@storeproduk')->name('customer.storeproduk');
        Route::post('/hapusproduk', 'FrontController@hapusproduk')->name('customer.hapusproduk');
        Route::get('/editproduk/{id}', 'FrontController@editproduk')->name('customer.editproduk');
        Route::put('/updateproduk/{id}', 'FrontController@updateproduk')->name('customer.updateproduk');

        //product order
        Route::group(['prefix' => 'orderstoko'], function() {
            Route::get('/{invoice}', 'FrontController@view')->name('orderstoko.view');
            Route::get('/payment/{invoice}', 'FrontController@acceptPayment')->name('orderstoko.approve_payment');
            Route::post('/shipping', 'FrontController@shippingOrder')->name('orderstoko.shipping');
            Route::delete('/{id}', 'FrontController@destroy')->name('orderstoko.destroy');
            Route::get('/return/{invoice}', 'FrontController@return')->name('orderstoko.return');
            Route::post('/return', 'FrontController@approveReturn')->name('orderstoko.approve_return');
        });

        //laporan order
        Route::group(['prefix' => 'reportstoko'], function() {
            Route::get('/order/pdf/{daterange}', 'FrontController@orderReportPdf')->name('reportstoko.order_pdf');
            Route::get('/return/pdf/{daterange}', 'FrontController@returnReportPdf')->name('reportstoko.return_pdf');
        });

    });
});

Auth::routes();
//cara akses dashboard admin http://127.0.0.1:8000/login email : bungmohammad@gmail.com password : secret
Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('category', 'CategoryController')->except(['create', 'show']);
    Route::resource('product', 'ProductController')->except(['show']);
    Route::get('/product/bulk', 'ProductController@massUploadForm')->name('product.bulk');
    Route::post('/product/bulk', 'ProductController@massUpload')->name('product.saveBulk');
    Route::post('/product/marketplace', 'ProductController@uploadViaMarketplace')->name('product.marketplace');

    Route::get('/pesan', 'HomeController@pesan')->name('pesan');

    Route::get('/artikel', 'ArtikelController@index')->name('artikel.index');
    Route::get('/artikel/tambahartikel', 'ArtikelController@tambahartikel')->name('artikel.tambahartikel');
    Route::post('/artikel/store','ArtikelController@store')->name('artikel.store');
    Route::post('/artikel/hapusartikel', 'ArtikelController@hapusartikel')->name('artikel.hapusartikel');
    Route::get('/artikel/editartikel/{id}', 'ArtikelController@editartikel')->name('artikel.editartikel');
    Route::put('/artikel/updateartikel/{id}', 'ArtikelController@updateartikel')->name('artikel.updateartikel');

    Route::get('/aktivasitoko', 'AktivasiController@index')->name('aktivasitoko.index');
    Route::get('/aktivasitoko/detail/{id}', 'AktivasiController@detail')->name('aktivasitoko.detail');
    Route::post('/aktivasitoko/hapus', 'AktivasiController@hapus')->name('aktivasitoko.hapus');
    Route::post('/aktivasitoko/terima', 'AktivasiController@terima')->name('aktivasitoko.terima');
    Route::post('/aktivasitoko/tolak', 'AktivasiController@tolak')->name('aktivasitoko.tolak');

    Route::group(['prefix' => 'orders'], function() {
        Route::get('/', 'OrderController@index')->name('orders.index');
        Route::get('/{invoice}', 'OrderController@view')->name('orders.view');
        Route::get('/payment/{invoice}', 'OrderController@acceptPayment')->name('orders.approve_payment');
        Route::post('/shipping', 'OrderController@shippingOrder')->name('orders.shipping');
        Route::delete('/{id}', 'OrderController@destroy')->name('orders.destroy');
        Route::get('/return/{invoice}', 'OrderController@return')->name('orders.return');
        Route::post('/return', 'OrderController@approveReturn')->name('orders.approve_return');
    });

    Route::group(['prefix' => 'reports'], function() {
        Route::get('/order', 'HomeController@orderReport')->name('report.order');
        Route::get('/order/pdf/{daterange}', 'HomeController@orderReportPdf')->name('report.order_pdf');
        Route::get('/return', 'HomeController@returnReport')->name('report.return');
        Route::get('/return/pdf/{daterange}', 'HomeController@returnReportPdf')->name('report.return_pdf');
    });
});
